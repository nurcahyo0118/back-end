<?php

use Illuminate\Database\Seeder;
use App\Tenant;
use App\Permission;

class TenantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tenant = new Tenant;
        $tenant->name = 'Baba Studio';
        $tenant->description = '';
        $tenant->domain = env('HOST_NAME');
        $tenant->subdomain = null;
        $tenant->is_cname = 1;
        $tenant->company_logo = "wowowowowo";
        $tenant->author_id = 0;

        $tenant->save();

        /* SuperAdmin */
        $permission_1 = new Permission;

        $permission_1->read_role = true;
        $permission_1->update_role = true;

        $permission_1->read_user = true;
        $permission_1->create_user = true;
        $permission_1->update_user = true;
        $permission_1->delete_user = true;

        $permission_1->read_course = true;
        $permission_1->create_course = true;
        $permission_1->update_course = true;
        $permission_1->delete_course = true;

        $permission_1->read_category = true;
        $permission_1->create_category = true;
        $permission_1->update_category = true;
        $permission_1->delete_category = true;

        $permission_1->read_tenant = true;
        $permission_1->create_tenant = true;
        $permission_1->update_tenant = true;
        $permission_1->delete_tenant = true;

        $permission_1->read_tenant_me = true;
        $permission_1->create_tenant_me = true;
        $permission_1->update_tenant_me = true;
        $permission_1->delete_tenant_me = true;

        $permission_1->role_id = 1;
        $permission_1->tenant_id = 1;

        $permission_1->save();


        /* Management */
        $permission_2 = new Permission;

        $permission_2->read_role = true;
        $permission_2->update_role = true;

        $permission_2->read_user = true;
        $permission_2->create_user = true;
        $permission_2->update_user = true;
        $permission_2->delete_user = true;

        $permission_2->read_course = true;
        $permission_2->create_course = true;
        $permission_2->update_course = true;
        $permission_2->delete_course = true;

        $permission_2->read_category = true;
        $permission_2->create_category = true;
        $permission_2->update_category = true;
        $permission_2->delete_category = true;

        $permission_2->read_tenant = true;
        $permission_2->create_tenant = true;
        $permission_2->update_tenant = true;
        $permission_2->delete_tenant = true;

        $permission_2->read_tenant_me = true;
        $permission_2->create_tenant_me = true;
        $permission_2->update_tenant_me = true;
        $permission_2->delete_tenant_me = true;

        $permission_2->role_id = 2;
        $permission_2->tenant_id = 1;

        $permission_2->save();


        /* Expert */
        $permission_3 = new Permission;

        $permission_3->read_role = true;
        $permission_3->update_role = true;

        $permission_3->read_user = true;
        $permission_3->create_user = true;
        $permission_3->update_user = true;
        $permission_3->delete_user = true;

        $permission_3->read_course = true;
        $permission_3->create_course = true;
        $permission_3->update_course = true;
        $permission_3->delete_course = true;

        $permission_3->read_category = true;
        $permission_3->create_category = true;
        $permission_3->update_category = true;
        $permission_3->delete_category = true;

        $permission_3->read_tenant = true;
        $permission_3->create_tenant = true;
        $permission_3->update_tenant = true;
        $permission_3->delete_tenant = true;

        $permission_3->read_tenant_me = true;
        $permission_3->create_tenant_me = true;
        $permission_3->update_tenant_me = true;
        $permission_3->delete_tenant_me = true;

        $permission_3->role_id = 3;
        $permission_3->tenant_id = 1;

        $permission_3->save();


        /* Instructur */
        $permission_4 = new Permission;

        $permission_4->read_role = true;
        $permission_4->update_role = true;

        $permission_4->read_user = true;
        $permission_4->create_user = true;
        $permission_4->update_user = true;
        $permission_4->delete_user = true;

        $permission_4->read_course = true;
        $permission_4->create_course = true;
        $permission_4->update_course = true;
        $permission_4->delete_course = true;

        $permission_4->read_category = true;
        $permission_4->create_category = true;
        $permission_4->update_category = true;
        $permission_4->delete_category = true;

        $permission_4->read_tenant = true;
        $permission_4->create_tenant = true;
        $permission_4->update_tenant = true;
        $permission_4->delete_tenant = true;

        $permission_4->read_tenant_me = true;
        $permission_4->create_tenant_me = true;
        $permission_4->update_tenant_me = true;
        $permission_4->delete_tenant_me = true;

        $permission_4->role_id = 4;
        $permission_4->tenant_id = 1;

        $permission_4->save();


        /* Murid *//* SuperAdmin */
        $permission_1 = new Permission;

        $permission_1->read_role = true;
        $permission_1->update_role = true;

        $permission_1->read_user = true;
        $permission_1->create_user = true;
        $permission_1->update_user = true;
        $permission_1->delete_user = true;

        $permission_1->read_course = true;
        $permission_1->create_course = true;
        $permission_1->update_course = true;
        $permission_1->delete_course = true;

        $permission_1->read_category = true;
        $permission_1->create_category = true;
        $permission_1->update_category = true;
        $permission_1->delete_category = true;

        $permission_1->read_tenant = true;
        $permission_1->create_tenant = true;
        $permission_1->update_tenant = true;
        $permission_1->delete_tenant = true;

        $permission_1->read_tenant_me = true;
        $permission_1->create_tenant_me = true;
        $permission_1->update_tenant_me = true;
        $permission_1->delete_tenant_me = true;

        $permission_1->role_id = 1;
        $permission_1->tenant_id = $tenant->id;

        $permission_1->save();


        /* Management */
        $permission_2 = new Permission;

        $permission_2->read_role = true;
        $permission_2->update_role = true;

        $permission_2->read_user = true;
        $permission_2->create_user = true;
        $permission_2->update_user = true;
        $permission_2->delete_user = true;

        $permission_2->read_course = true;
        $permission_2->create_course = true;
        $permission_2->update_course = true;
        $permission_2->delete_course = true;

        $permission_2->read_category = true;
        $permission_2->create_category = true;
        $permission_2->update_category = true;
        $permission_2->delete_category = true;

        $permission_2->read_tenant = true;
        $permission_2->create_tenant = true;
        $permission_2->update_tenant = true;
        $permission_2->delete_tenant = true;

        $permission_2->read_tenant_me = true;
        $permission_2->create_tenant_me = true;
        $permission_2->update_tenant_me = true;
        $permission_2->delete_tenant_me = true;

        $permission_2->role_id = 2;
        $permission_2->tenant_id = $tenant->id;

        $permission_2->save();


        /* Expert */
        $permission_3 = new Permission;

        $permission_3->read_role = true;
        $permission_3->update_role = true;

        $permission_3->read_user = true;
        $permission_3->create_user = true;
        $permission_3->update_user = true;
        $permission_3->delete_user = true;

        $permission_3->read_course = true;
        $permission_3->create_course = true;
        $permission_3->update_course = true;
        $permission_3->delete_course = true;

        $permission_3->read_category = true;
        $permission_3->create_category = true;
        $permission_3->update_category = true;
        $permission_3->delete_category = true;

        $permission_3->read_tenant = true;
        $permission_3->create_tenant = true;
        $permission_3->update_tenant = true;
        $permission_3->delete_tenant = true;

        $permission_3->read_tenant_me = true;
        $permission_3->create_tenant_me = true;
        $permission_3->update_tenant_me = true;
        $permission_3->delete_tenant_me = true;

        $permission_3->role_id = 3;
        $permission_3->tenant_id = $tenant->id;

        $permission_3->save();


        /* Instructur */
        $permission_4 = new Permission;/* SuperAdmin */
        $permission_1 = new Permission;

        $permission_1->read_role = true;
        $permission_1->update_role = true;

        $permission_1->read_user = true;
        $permission_1->create_user = true;
        $permission_1->update_user = true;
        $permission_1->delete_user = true;

        $permission_1->read_course = true;
        $permission_1->create_course = true;
        $permission_1->update_course = true;
        $permission_1->delete_course = true;

        $permission_1->read_category = true;
        $permission_1->create_category = true;
        $permission_1->update_category = true;
        $permission_1->delete_category = true;

        $permission_1->read_tenant = true;
        $permission_1->create_tenant = true;
        $permission_1->update_tenant = true;
        $permission_1->delete_tenant = true;

        $permission_1->read_tenant_me = true;
        $permission_1->create_tenant_me = true;
        $permission_1->update_tenant_me = true;
        $permission_1->delete_tenant_me = true;

        $permission_1->role_id = 1;
        $permission_1->tenant_id = $tenant->id;

        $permission_1->save();


        /* Management */
        $permission_2 = new Permission;

        $permission_2->read_role = true;
        $permission_2->update_role = true;

        $permission_2->read_user = true;
        $permission_2->create_user = true;
        $permission_2->update_user = true;
        $permission_2->delete_user = true;

        $permission_2->read_course = true;
        $permission_2->create_course = true;
        $permission_2->update_course = true;
        $permission_2->delete_course = true;

        $permission_2->read_category = true;
        $permission_2->create_category = true;
        $permission_2->update_category = true;
        $permission_2->delete_category = true;

        $permission_2->read_tenant = true;
        $permission_2->create_tenant = true;
        $permission_2->update_tenant = true;
        $permission_2->delete_tenant = true;

        $permission_2->read_tenant_me = true;/* SuperAdmin */
        $permission_1 = new Permission;

        $permission_1->read_role = true;
        $permission_1->update_role = true;

        $permission_1->read_user = true;
        $permission_1->create_user = true;
        $permission_1->update_user = true;
        $permission_1->delete_user = true;

        $permission_1->read_course = true;
        $permission_1->create_course = true;
        $permission_1->update_course = true;
        $permission_1->delete_course = true;

        $permission_1->read_category = true;
        $permission_1->create_category = true;
        $permission_1->update_category = true;
        $permission_1->delete_category = true;

        $permission_1->read_tenant = true;
        $permission_1->create_tenant = true;
        $permission_1->update_tenant = true;
        $permission_1->delete_tenant = true;

        $permission_1->read_tenant_me = true;
        $permission_1->create_tenant_me = true;
        $permission_1->update_tenant_me = true;
        $permission_1->delete_tenant_me = true;

        $permission_1->role_id = 1;
        $permission_1->tenant_id = $tenant->id;

        $permission_1->save();


        /* Management */
        $permission_2 = new Permission;

        $permission_2->read_role = true;
        $permission_2->update_role = true;

        $permission_2->read_user = true;
        $permission_2->create_user = true;
        $permission_2->update_user = true;
        $permission_2->delete_user = true;

        $permission_2->read_course = true;
        $permission_2->create_course = true;
        $permission_2->update_course = true;
        $permission_2->delete_course = true;

        $permission_2->read_category = true;
        $permission_2->create_category = true;
        $permission_2->update_category = true;
        $permission_2->delete_category = true;

        $permission_2->read_tenant = true;
        $permission_2->create_tenant = true;
        $permission_2->update_tenant = true;
        $permission_2->delete_tenant = true;

        $permission_2->read_tenant_me = true;
        $permission_2->create_tenant_me = true;
        $permission_2->update_tenant_me = true;
        $permission_2->delete_tenant_me = true;

        $permission_2->role_id = 2;
        $permission_2->tenant_id = $tenant->id;

        $permission_2->save();


        /* Expert */
        $permission_3 = new Permission;

        $permission_3->read_role = true;
        $permission_3->update_role = true;

        $permission_3->read_user = true;
        $permission_3->create_user = true;
        $permission_3->update_user = true;
        $permission_3->delete_user = true;

        $permission_3->read_course = true;
        $permission_3->create_course = true;
        $permission_3->update_course = true;
        $permission_3->delete_course = true;

        $permission_3->read_category = true;
        $permission_3->create_category = true;
        $permission_3->update_category = true;
        $permission_3->delete_category = true;

        $permission_3->read_tenant = true;
        $permission_3->create_tenant = true;
        $permission_3->update_tenant = true;
        $permission_3->delete_tenant = true;

        $permission_3->read_tenant_me = true;
        $permission_3->create_tenant_me = true;
        $permission_3->update_tenant_me = true;
        $permission_3->delete_tenant_me = true;

        $permission_3->role_id = 3;
        $permission_3->tenant_id = $tenant->id;

        $permission_3->save();


        /* Instructur */
        $permission_4 = new Permission;

        $permission_4->read_role = true;
        $permission_4->update_role = true;

        $permission_4->read_user = true;
        $permission_4->create_user = true;
        $permission_4->update_user = true;
        $permission_4->delete_user = true;

        $permission_4->read_course = true;
        $permission_4->create_course = true;
        $permission_4->update_course = true;
        $permission_4->delete_course = true;

        $permission_4->read_category = true;
        $permission_4->create_category = true;
        $permission_4->update_category = true;
        $permission_4->delete_category = true;

        $permission_4->read_tenant = true;
        $permission_4->create_tenant = true;
        $permission_4->update_tenant = true;
        $permission_4->delete_tenant = true;

        $permission_4->read_tenant_me = true;
        $permission_4->create_tenant_me = true;
        $permission_4->update_tenant_me = true;
        $permission_4->delete_tenant_me = true;

        $permission_4->role_id = 4;/* SuperAdmin */
        $permission_1 = new Permission;

        $permission_1->read_role = true;
        $permission_1->update_role = true;

        $permission_1->read_user = true;
        $permission_1->create_user = true;
        $permission_1->update_user = true;
        $permission_1->delete_user = true;

        $permission_1->read_course = true;
        $permission_1->create_course = true;
        $permission_1->update_course = true;
        $permission_1->delete_course = true;

        $permission_1->read_category = true;
        $permission_1->create_category = true;
        $permission_1->update_category = true;
        $permission_1->delete_category = true;

        $permission_1->read_tenant = true;
        $permission_1->create_tenant = true;
        $permission_1->update_tenant = true;
        $permission_1->delete_tenant = true;

        $permission_1->read_tenant_me = true;
        $permission_1->create_tenant_me = true;
        $permission_1->update_tenant_me = true;
        $permission_1->delete_tenant_me = true;

        $permission_1->role_id = 1;
        $permission_1->tenant_id = $tenant->id;

        $permission_1->save();


        /* Management */
        $permission_2 = new Permission;

        $permission_2->read_role = true;
        $permission_2->update_role = true;

        $permission_2->read_user = true;
        $permission_2->create_user = true;
        $permission_2->update_user = true;
        $permission_2->delete_user = true;

        $permission_2->read_course = true;
        $permission_2->create_course = true;
        $permission_2->update_course = true;
        $permission_2->delete_course = true;

        $permission_2->read_category = true;
        $permission_2->create_category = true;
        $permission_2->update_category = true;
        $permission_2->delete_category = true;

        $permission_2->read_tenant = true;
        $permission_2->create_tenant = true;
        $permission_2->update_tenant = true;
        $permission_2->delete_tenant = true;

        $permission_2->read_tenant_me = true;
        $permission_2->create_tenant_me = true;
        $permission_2->update_tenant_me = true;
        $permission_2->delete_tenant_me = true;

        $permission_2->role_id = 2;
        $permission_2->tenant_id = $tenant->id;

        $permission_2->save();


        /* Expert */
        $permission_3 = new Permission;

        $permission_3->read_role = true;
        $permission_3->update_role = true;

        $permission_3->read_user = true;
        $permission_3->create_user = true;
        $permission_3->update_user = true;
        $permission_3->delete_user = true;

        $permission_3->read_course = true;
        $permission_3->create_course = true;
        $permission_3->update_course = true;
        $permission_3->delete_course = true;

        $permission_3->read_category = true;
        $permission_3->create_category = true;
        $permission_3->update_category = true;
        $permission_3->delete_category = true;

        $permission_3->read_tenant = true;
        $permission_3->create_tenant = true;
        $permission_3->update_tenant = true;
        $permission_3->delete_tenant = true;

        $permission_3->read_tenant_me = true;
        $permission_3->create_tenant_me = true;
        $permission_3->update_tenant_me = true;
        $permission_3->delete_tenant_me = true;

        $permission_3->role_id = 3;
        $permission_3->tenant_id = $tenant->id;

        $permission_3->save();


        /* Instructur */
        $permission_4 = new Permission;

        $permission_4->read_role = true;
        $permission_4->update_role = true;

        $permission_4->read_user = true;
        $permission_4->create_user = true;
        $permission_4->update_user = true;
        $permission_4->delete_user = true;

        $permission_4->read_course = true;
        $permission_4->create_course = true;
        $permission_4->update_course = true;
        $permission_4->delete_course = true;

        $permission_4->read_category = true;
        $permission_4->create_category = true;
        $permission_4->update_category = true;
        $permission_4->delete_category = true;

        $permission_4->read_tenant = true;
        $permission_4->create_tenant = true;
        $permission_4->update_tenant = true;
        $permission_4->delete_tenant = true;

        $permission_4->read_tenant_me = true;
        $permission_4->create_tenant_me = true;
        $permission_4->update_tenant_me = true;
        $permission_4->delete_tenant_me = true;

        $permission_4->role_id = 4;
        $permission_4->tenant_id = $tenant->id;

        $permission_4->save();


        /* Murid */
        $permission_5 = new Permission;

        $permission_5->read_role = false;
        $permission_5->update_role = false;

        $permission_5->read_user = false;
        $permission_5->create_user = false;
        $permission_5->update_user = false;
        $permission_5->delete_user = false;

        $permission_5->read_course = false;
        $permission_5->create_course = false;
        $permission_5->update_course = false;
        $permission_5->delete_course = false;

        $permission_5->read_category = false;
        $permission_5->create_category = false;
        $permission_5->update_category = false;
        $permission_5->delete_category = false;

        $permission_5->read_tenant = false;
        $permission_5->create_tenant = false;
        $permission_5->update_tenant = false;
        $permission_5->delete_tenant = false;

        $permission_5->read_tenant_me = false;
        $permission_5->create_tenant_me = false;
        $permission_5->update_tenant_me = false;
        $permission_5->delete_tenant_me = false;

        $permission_5->role_id = 5;
        $permission_5->tenant_id = $tenant->id;

        $permission_5->save();
        $permission_4->tenant_id = $tenant->id;

        $permission_4->save();


        /* Murid */
        $permission_5 = new Permission;

        $permission_5->read_role = false;
        $permission_5->update_role = false;

        $permission_5->read_user = false;
        $permission_5->create_user = false;
        $permission_5->update_user = false;
        $permission_5->delete_user = false;

        $permission_5->read_course = false;
        $permission_5->create_course = false;
        $permission_5->update_course = false;
        $permission_5->delete_course = false;

        $permission_5->read_category = false;
        $permission_5->create_category = false;
        $permission_5->update_category = false;
        $permission_5->delete_category = false;

        $permission_5->read_tenant = false;
        $permission_5->create_tenant = false;
        $permission_5->update_tenant = false;
        $permission_5->delete_tenant = false;

        $permission_5->read_tenant_me = false;
        $permission_5->create_tenant_me = false;
        $permission_5->update_tenant_me = false;
        $permission_5->delete_tenant_me = false;

        $permission_5->role_id = 5;
        $permission_5->tenant_id = $tenant->id;

        $permission_5->save();
        $permission_2->create_tenant_me = true;
        $permission_2->update_tenant_me = true;
        $permission_2->delete_tenant_me = true;

        $permission_2->role_id = 2;
        $permission_2->tenant_id = $tenant->id;

        $permission_2->save();


        /* Expert */
        $permission_3 = new Permission;

        $permission_3->read_role = true;
        $permission_3->update_role = true;

        $permission_3->read_user = true;
        $permission_3->create_user = true;
        $permission_3->update_user = true;
        $permission_3->delete_user = true;

        $permission_3->read_course = true;
        $permission_3->create_course = true;
        $permission_3->update_course = true;
        $permission_3->delete_course = true;

        $permission_3->read_category = true;
        $permission_3->create_category = true;
        $permission_3->update_category = true;
        $permission_3->delete_category = true;

        $permission_3->read_tenant = true;
        $permission_3->create_tenant = true;
        $permission_3->update_tenant = true;
        $permission_3->delete_tenant = true;

        $permission_3->read_tenant_me = true;
        $permission_3->create_tenant_me = true;
        $permission_3->update_tenant_me = true;
        $permission_3->delete_tenant_me = true;

        $permission_3->role_id = 3;
        $permission_3->tenant_id = $tenant->id;

        $permission_3->save();


        /* Instructur */
        $permission_4 = new Permission;

        $permission_4->read_role = true;
        $permission_4->update_role = true;

        $permission_4->read_user = true;
        $permission_4->create_user = true;
        $permission_4->update_user = true;
        $permission_4->delete_user = true;

        $permission_4->read_course = true;
        $permission_4->create_course = true;
        $permission_4->update_course = true;
        $permission_4->delete_course = true;

        $permission_4->read_category = true;
        $permission_4->create_category = true;
        $permission_4->update_category = true;
        $permission_4->delete_category = true;

        $permission_4->read_tenant = true;
        $permission_4->create_tenant = true;
        $permission_4->update_tenant = true;
        $permission_4->delete_tenant = true;

        $permission_4->read_tenant_me = true;
        $permission_4->create_tenant_me = true;
        $permission_4->update_tenant_me = true;
        $permission_4->delete_tenant_me = true;

        $permission_4->role_id = 4;
        $permission_4->tenant_id = $tenant->id;

        $permission_4->save();


        /* Murid */
        $permission_5 = new Permission;

        $permission_5->read_role = false;
        $permission_5->update_role = false;

        $permission_5->read_user = false;
        $permission_5->create_user = false;
        $permission_5->update_user = false;
        $permission_5->delete_user = false;

        $permission_5->read_course = false;
        $permission_5->create_course = false;
        $permission_5->update_course = false;
        $permission_5->delete_course = false;

        $permission_5->read_category = false;
        $permission_5->create_category = false;
        $permission_5->update_category = false;
        $permission_5->delete_category = false;

        $permission_5->read_tenant = false;
        $permission_5->create_tenant = false;
        $permission_5->update_tenant = false;
        $permission_5->delete_tenant = false;

        $permission_5->read_tenant_me = false;
        $permission_5->create_tenant_me = false;
        $permission_5->update_tenant_me = false;
        $permission_5->delete_tenant_me = false;

        $permission_5->role_id = 5;
        $permission_5->tenant_id = $tenant->id;

        $permission_5->save();

        $permission_4->read_role = true;
        $permission_4->update_role = true;

        $permission_4->read_user = true;
        $permission_4->create_user = true;
        $permission_4->update_user = true;
        $permission_4->delete_user = true;

        $permission_4->read_course = true;
        $permission_4->create_course = true;
        $permission_4->update_course = true;
        $permission_4->delete_course = true;

        $permission_4->read_category = true;
        $permission_4->create_category = true;
        $permission_4->update_category = true;
        $permission_4->delete_category = true;

        $permission_4->read_tenant = true;
        $permission_4->create_tenant = true;
        $permission_4->update_tenant = true;
        $permission_4->delete_tenant = true;

        $permission_4->read_tenant_me = true;
        $permission_4->create_tenant_me = true;
        $permission_4->update_tenant_me = true;
        $permission_4->delete_tenant_me = true;

        $permission_4->role_id = 4;
        $permission_4->tenant_id = $tenant->id;

        $permission_4->save();


        /* Murid */
        $permission_5 = new Permission;

        $permission_5->read_role = false;
        $permission_5->update_role = false;

        $permission_5->read_user = false;
        $permission_5->create_user = false;
        $permission_5->update_user = false;
        $permission_5->delete_user = false;

        $permission_5->read_course = false;
        $permission_5->create_course = false;
        $permission_5->update_course = false;
        $permission_5->delete_course = false;

        $permission_5->read_category = false;
        $permission_5->create_category = false;
        $permission_5->update_category = false;
        $permission_5->delete_category = false;

        $permission_5->read_tenant = false;
        $permission_5->create_tenant = false;
        $permission_5->update_tenant = false;
        $permission_5->delete_tenant = false;

        $permission_5->read_tenant_me = false;
        $permission_5->create_tenant_me = false;
        $permission_5->update_tenant_me = false;
        $permission_5->delete_tenant_me = false;

        $permission_5->role_id = 5;
        $permission_5->tenant_id = $tenant->id;

        $permission_5->save();
        $permission_5 = new Permission;

        $permission_5->read_role = false;
        $permission_5->update_role = false;

        $permission_5->read_user = false;
        $permission_5->create_user = false;
        $permission_5->update_user = false;
        $permission_5->delete_user = false;

        $permission_5->read_course = false;
        $permission_5->create_course = false;
        $permission_5->update_course = false;
        $permission_5->delete_course = false;

        $permission_5->read_category = false;
        $permission_5->create_category = false;
        $permission_5->update_category = false;
        $permission_5->delete_category = false;

        $permission_5->read_tenant = false;
        $permission_5->create_tenant = false;
        $permission_5->update_tenant = false;
        $permission_5->delete_tenant = false;

        $permission_5->read_tenant_me = false;
        $permission_5->create_tenant_me = false;
        $permission_5->update_tenant_me = false;
        $permission_5->delete_tenant_me = false;

        $permission_5->role_id = 5;
        $permission_5->tenant_id = 1;

        $permission_5->save();
    }
}
