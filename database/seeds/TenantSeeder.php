<?php

use Illuminate\Database\Seeder;
use App\Tenant;

class TenantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tenant = new Tenant;
        $tenant->name = 'Baba Studio';
        $tenant->description = '';
        $tenant->domain = env('HOST_NAME');
        $tenant->subdomain = null;
        $tenant->is_cname = 1;
        $tenant->company_logo = "wowowowowo";
        $tenant->author_id = 0;

        $tenant->save();
    }
}
