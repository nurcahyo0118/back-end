<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    public function material()
    {
        return $this->hasOne('App\Material', 'id', 'material_id');
    }
}
