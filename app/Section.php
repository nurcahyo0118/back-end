<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    public function course()
    {
        return $this->hasOne('App\Course', 'id', 'course_id');
    }

    public function materials()
    {
        return $this->hasMany('App\Material', 'section_id');
    }

}
