<?php

namespace App\Http\Middleware;

use App\Tenant;
use Closure;

class Domain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();

        /* Kecuali BabaStudio */
        if ($_SERVER['HTTP_HOST'] === env('HOST_NAME')) {
            return $next($request);
        }

        if (!$tenant) {
            return redirect()->route('notfound');
        }

        return $next($request);
    }
}
