<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Tenant;
use App\User;
use Illuminate\Support\Facades\Log;
use Session;

class TenantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($domain)
    {
        $tenants = Tenant::all();
        return view('tenants.index')
            ->withTenants($tenants);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($domain)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
//        Validate request
        if ((int)$request->is_cname === 1) {
            /* DOMAIN */
            $this->validate($request, [
                'name' => 'required|max:150|unique:tenants',
                'domain' => 'max:50|unique:tenants'
            ]);
        } else {
            /* SUBDOMAIN */
            $this->validate($request, [
                'name' => 'required|max:150|unique:tenants',
                'subdomain' => 'max:10|unique:tenants'
            ]);
        }

//        Save Tenant to database
        $tenant = new Tenant;
        $tenant->name = $request->name;
        $tenant->description = $request->description;

        if ((int)$request->is_cname === 1) {
            $tenant->domain = $request->domain;
            $tenant->subdomain = null;
        } else {
            $tenant->domain = null;
            $tenant->subdomain = $request->subdomain;
        }

        $tenant->is_cname = (int)$request->is_cname;
        $tenant->company_logo = "wowowowowo";
        $tenant->author_id = Auth::id();

        $tenant->save();

//        add success message with session flash (hanya untuk 1 page)
        Session::flash('success', 'Tenant berhasil di tambahkan !');

//        refresh the page
        return redirect()->back();
    }

    public function show($domain, $id)
    {
        $tenant = Tenant::find($id);

        $users = User::all();

        return view('tenants.detail')
            ->withTenant($tenant)
            ->withUsers($users);
    }

    public function me($domain)
    {
        $tenant = Tenant::where(['domain' => $domain])->first();

        $users = User::where(['tenant_id' => $tenant->id])->get();

        return view('tenants.me')
            ->withTenant($tenant)
            ->withUsers($users);
    }

    public function selectExpertPage($domain, $id)
    {
        $tenant = Tenant::find($id);

        $users = User::where(['tenant_id' => 1])->get();

        $experts = User::where(['role_id' => 3, 'tenant_id' => $tenant->id])->get();

        return view('tenants.selectexpert')
            ->withTenant($tenant)
            ->withUsers($users)
            ->withExperts($experts)
            ->withId($id);
    }

    public function selectExpert($domain, $id, Request $request)
    {

        $user = User::find($request->user_id);

        $user->tenant_id = $id;
        $user->role_id = 3;

        $user->save();

        return redirect()->back();
    }

    public function unselectExpert($domain, $id, Request $request)
    {

        $user = User::find($request->user_id);

        $user->tenant_id = 1;
        $user->role_id = 5;

        $user->save();

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, $id)
    {
        $tenant = Tenant::find($id);

        return view('tenants.edit')
            ->withTenant($tenant);
    }

    public function meEdit($domain)
    {
        $tenant = Tenant::where(['domain' => $domain])->first();

        return view('tenants.meedit')
            ->withTenant($tenant);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        //        Validate request
        if ((int)$request->is_cname === 1) {
            /* DOMAIN */
            $this->validate($request, [
                'name' => 'required|max:150|unique:tenants,name,' . $id,
                'domain' => 'max:50|unique:tenants,domain,' . $id
            ]);
        } else {
            /* SUBDOMAIN */
            $this->validate($request, [
                'name' => 'required|max:150|unique:tenants,name,' . $id,
                'subdomain' => 'max:10|unique:tenants,subdomain,' . $id
            ]);
        }

//        Save Tenant to database
        $tenant = Tenant::find($id);
        Log::debug($tenant);
        $tenant->name = $request->name;
        $tenant->description = $request->description;

        if ((int)$request->is_cname === 1) {
            $tenant->domain = $request->domain;
            $tenant->subdomain = null;
        } else {
            $tenant->domain = null;
            $tenant->subdomain = $request->subdomain;
        }

        $tenant->is_cname = (int)$request->is_cname;
        $tenant->company_logo = "wowowowowo";
        $tenant->author_id = Auth::id();

        $tenant->save();

//        add success message with session flash (hanya untuk 1 page)
        Session::flash('success', 'Tenant berhasil di edit !');

//        refresh the page
        return redirect()->route('tenants.index', $domain);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tenant = Tenant::find($id);

//        if ($tenant->file != null) {
//            unlink('file/research/'.$tenant->file);
//        }

        $tenant->delete();

        return redirect()->back();
    }
}
