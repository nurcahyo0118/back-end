<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;

use App\User;


class LoginController extends Controller
{
    public function postLogin(Request $request)
    {
      $http = new Client;

      $response = $http->request('POST', 'http://' . env('HOST_NAME') . '/oauth/token', [
          'form_params' => [
            'grant_type'=> 'password',
            'client_id'=> 4,
            'client_secret'=> 'k9WRUa6CvaxapVKxHnNgmjddPIWazZRormuMm3py',
            'username'=> $request->username,
            'password'=> $request->password
          ]
      ]);

      return json_decode((string) $response->getBody(), true);
    }

    public function postRegister(Request $request)
    {
      $user = new User;

      $user->fullname = $request->fullname;
      $user->username = $request->username;
      $user->password = Hash::make($request->password);
      $user->email = $request->email;

      $user->role_id = 5;

      $user->branch_id = 0;
      $user->tenant_id = $request->tenant_id;

      $user->save();

      return response()->json($user);
    }
}
