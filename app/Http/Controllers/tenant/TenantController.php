<?php

namespace App\Http\Controllers\tenant;

use App\Permission;
use Illuminate\Http\Request;

use Auth;
use App\Tenant;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Session;

class TenantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($subdomain)
    {
        $tenants = Tenant::all();
        return view('tenants.index')
            ->withTenants($tenants);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($subdomain)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($subdomain, Request $request)
    {
//        Validate request
        if ((int)$request->is_cname === 1) {
            /* subdomain */
            $this->validate($request, [
                'name' => 'required|max:150|unique:tenants',
                'subdomain' => 'max:50|unique:tenants'
            ]);
        } else {
            /* SUBDOMAIN */
            $this->validate($request, [
                'name' => 'required|max:150|unique:tenants',
                'subdomain' => 'max:10|unique:tenants'
            ]);
        }

//        Save Tenant to database
        $tenant = new Tenant;
        $tenant->name = $request->name;
        $tenant->description = $request->description;

        if ((int)$request->is_cname === 1) {
            $tenant->subdomain = $request->subdomain;
            $tenant->subdomain = null;
        } else {
            $tenant->subdomain = null;
            $tenant->subdomain = $request->subdomain;
        }

        $tenant->is_cname = (int)$request->is_cname;
        $tenant->company_logo = "wowowowowo";
        $tenant->author_id = Auth::id();

        $tenant->save();

        /* SuperAdmin */
        $permission_1 = new Permission;

        $permission_1->read_role = true;
        $permission_1->update_role = true;

        $permission_1->read_user = true;
        $permission_1->create_user = true;
        $permission_1->update_user = true;
        $permission_1->delete_user = true;

        $permission_1->read_course = true;
        $permission_1->create_course = true;
        $permission_1->update_course = true;
        $permission_1->delete_course = true;

        $permission_1->read_category = true;
        $permission_1->create_category = true;
        $permission_1->update_category = true;
        $permission_1->delete_category = true;

        $permission_1->read_tenant = true;
        $permission_1->create_tenant = true;
        $permission_1->update_tenant = true;
        $permission_1->delete_tenant = true;

        $permission_1->read_tenant_me = true;
        $permission_1->create_tenant_me = true;
        $permission_1->update_tenant_me = true;
        $permission_1->delete_tenant_me = true;

        $permission_1->role_id = 1;
        $permission_1->tenant_id = $tenant->id;

        $permission_1->save();


        /* Management */
        $permission_2 = new Permission;

        $permission_2->read_role = true;
        $permission_2->update_role = true;

        $permission_2->read_user = true;
        $permission_2->create_user = true;
        $permission_2->update_user = true;
        $permission_2->delete_user = true;

        $permission_2->read_course = true;
        $permission_2->create_course = true;
        $permission_2->update_course = true;
        $permission_2->delete_course = true;

        $permission_2->read_category = true;
        $permission_2->create_category = true;
        $permission_2->update_category = true;
        $permission_2->delete_category = true;

        $permission_2->read_tenant = true;
        $permission_2->create_tenant = true;
        $permission_2->update_tenant = true;
        $permission_2->delete_tenant = true;

        $permission_2->read_tenant_me = true;
        $permission_2->create_tenant_me = true;
        $permission_2->update_tenant_me = true;
        $permission_2->delete_tenant_me = true;

        $permission_2->role_id = 2;
        $permission_2->tenant_id = $tenant->id;

        $permission_2->save();


        /* Expert */
        $permission_3 = new Permission;

        $permission_3->read_role = true;
        $permission_3->update_role = true;

        $permission_3->read_user = true;
        $permission_3->create_user = true;
        $permission_3->update_user = true;
        $permission_3->delete_user = true;

        $permission_3->read_course = true;
        $permission_3->create_course = true;
        $permission_3->update_course = true;
        $permission_3->delete_course = true;

        $permission_3->read_category = true;
        $permission_3->create_category = true;
        $permission_3->update_category = true;
        $permission_3->delete_category = true;

        $permission_3->read_tenant = true;
        $permission_3->create_tenant = true;
        $permission_3->update_tenant = true;
        $permission_3->delete_tenant = true;

        $permission_3->read_tenant_me = true;
        $permission_3->create_tenant_me = true;
        $permission_3->update_tenant_me = true;
        $permission_3->delete_tenant_me = true;

        $permission_3->role_id = 3;
        $permission_3->tenant_id = $tenant->id;

        $permission_3->save();


        /* Instructur */
        $permission_4 = new Permission;

        $permission_4->read_role = true;
        $permission_4->update_role = true;

        $permission_4->read_user = true;
        $permission_4->create_user = true;
        $permission_4->update_user = true;
        $permission_4->delete_user = true;

        $permission_4->read_course = true;
        $permission_4->create_course = true;
        $permission_4->update_course = true;
        $permission_4->delete_course = true;

        $permission_4->read_category = true;
        $permission_4->create_category = true;
        $permission_4->update_category = true;
        $permission_4->delete_category = true;

        $permission_4->read_tenant = true;
        $permission_4->create_tenant = true;
        $permission_4->update_tenant = true;
        $permission_4->delete_tenant = true;

        $permission_4->read_tenant_me = true;
        $permission_4->create_tenant_me = true;
        $permission_4->update_tenant_me = true;
        $permission_4->delete_tenant_me = true;

        $permission_4->role_id = 4;
        $permission_4->tenant_id = $tenant->id;

        $permission_4->save();


        /* Murid */
        $permission_5 = new Permission;

        $permission_5->read_role = false;
        $permission_5->update_role = false;

        $permission_5->read_user = false;
        $permission_5->create_user = false;
        $permission_5->update_user = false;
        $permission_5->delete_user = false;

        $permission_5->read_course = false;
        $permission_5->create_course = false;
        $permission_5->update_course = false;
        $permission_5->delete_course = false;

        $permission_5->read_category = false;
        $permission_5->create_category = false;
        $permission_5->update_category = false;
        $permission_5->delete_category = false;

        $permission_5->read_tenant = false;
        $permission_5->create_tenant = false;
        $permission_5->update_tenant = false;
        $permission_5->delete_tenant = false;

        $permission_5->read_tenant_me = false;
        $permission_5->create_tenant_me = false;
        $permission_5->update_tenant_me = false;
        $permission_5->delete_tenant_me = false;

        $permission_5->role_id = 5;
        $permission_5->tenant_id = $tenant->id;

        $permission_5->save();

//        add success message with session flash (hanya untuk 1 page)
        Session::flash('success', 'Tenant berhasil di tambahkan !');

//        refresh the page
        return redirect()->back();
    }

    public function show($subdomain, $id)
    {
        $tenant = Tenant::find($id);

        $users = User::all();

        return view('tenants.detail')
            ->withTenant($tenant)
            ->withUsers($users);
    }

    public function me($subdomain)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        $users = User::where(['tenant_id' => $tenant->id])->get();

        return view('tenants.me')
            ->withTenant($tenant)
            ->withUsers($users);
    }

    public function tenantMe($subdomain)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        $users = User::where(['tenant_id' => $tenant->id])->get();

        return view('tenants.me')
            ->withTenant($tenant)
            ->withUsers($users);
    }

    public function selectExpertPage($subdomain, $id)
    {
        $tenant = Tenant::find($id);

        $users = User::where(['tenant_id' => 1])->get();

        $experts = User::where(['role_id' => 3, 'tenant_id' => $tenant->id])->get();

        return view('tenants.selectexpert')
            ->withTenant($tenant)
            ->withUsers($users)
            ->withExperts($experts)
            ->withId($id);
    }

    public function selectExpert($subdomain, $id, Request $request)
    {

        $user = User::find($request->user_id);

        $user->tenant_id = $id;
        $user->role_id = 3;

        $user->save();

        return redirect()->back();
    }

    public function unselectExpert($subdomain, $id, Request $request)
    {

        $user = User::find($request->user_id);

        $user->tenant_id = 1;
        $user->role_id = 5;

        $user->save();

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($subdomain, $id)
    {
        $tenant = Tenant::find($id);

        return view('tenants.edit')
            ->withTenant($tenant);
    }

    public function meEdit($subdomain)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        return view('tenants.meedit')
            ->withTenant($tenant);
    }

    public function tenantMeEdit($subdomain)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        return view('tenants.meedit')
            ->withTenant($tenant);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($subdomain, Request $request, $id)
    {
        //        Validate request
        if ((int)$request->is_cname === 1) {
            /* subdomain */
            $this->validate($request, [
                'name' => 'required|max:150|unique:tenants,name,' . $id,
                'subdomain' => 'max:50|unique:tenants,subdomain,' . $id
            ]);
        } else {
            /* SUBDOMAIN */
            $this->validate($request, [
                'name' => 'required|max:150|unique:tenants,name,' . $id,
                'subdomain' => 'max:10|unique:tenants,subdomain,' . $id
            ]);
        }

//        Save Tenant to database
        $tenant = Tenant::find($id);
        Log::debug($tenant);
        $tenant->name = $request->name;
        $tenant->description = $request->description;

        if ((int)$request->is_cname === 1) {
            $tenant->subdomain = $request->subdomain;
            $tenant->subdomain = null;
        } else {
            $tenant->subdomain = null;
            $tenant->subdomain = $request->subdomain;
        }

        $tenant->is_cname = (int)$request->is_cname;
        $tenant->company_logo = "wowowowowo";
        $tenant->author_id = Auth::id();

        $tenant->save();

//        add success message with session flash (hanya untuk 1 page)
//        Session::flash('success', 'Tenant berhasil di edit !');

        if ($subdomain != env('HOST_NAME')) {
            Auth::guard('web')->logout();

            if ($tenant->is_cname) {
                return Redirect::to('http://' . $tenant->subdomain);
            } else {
                return redirect()->route('tenant.landing', $tenant->subdomain);
            }

        } else {
            return redirect()->route('tenants.index', $subdomain);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($subdomain, $id)
    {
        $tenant = Tenant::find($id);

//        if ($tenant->file != null) {
//            unlink('file/research/'.$tenant->file);
//        }

        $tenant->delete();

        return redirect()->back();
    }
}
