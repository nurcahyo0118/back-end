<?php

namespace App\Http\Controllers\tenant;

use Illuminate\Http\Request;
use App\Quiz;

use Log;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($subdomain, Request $request)
    {
        Log::debug($request);
        $quizzes = Quiz::where(['section_id' => $request->section_id])->get();

        $quiz = new Quiz;
        $quiz->question = $request->question;
        $quiz->option_a = $request->option_a;
        $quiz->option_b = $request->option_b;
        $quiz->option_c = $request->option_c;
        $quiz->option_d = $request->option_d;
        $quiz->option_true = $request->option_true;
        $quiz->line = $quizzes->count() + 1;
        $quiz->section_id = $request->section_id;

        $quiz->save();

        return view('courses.childquiz')->withQuiz($quiz);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($subdomain, Request $request, $id)
    {
      Log::debug($request->question);
      $quiz = Quiz::find($id);
      Log::debug($quiz);

      $quiz->question = $request->question;
      $quiz->option_a = $request->option_a;
      $quiz->option_b = $request->option_b;
      $quiz->option_c = $request->option_c;
      $quiz->option_d = $request->option_d;
      $quiz->option_true = $request->option_true;

      $quiz->save();

      return view('courses.childquiz')->withQuiz($quiz);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($subdomain, $id)
    {
      $quiz = Quiz::find($id);
      $quiz->delete();
    }
}
