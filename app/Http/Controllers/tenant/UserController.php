<?php

namespace App\Http\Controllers\tenant;

use App\Role;
use App\Tenant;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($subdomain)
    {
        $tenant = Tenant::where('subdomain', $subdomain)->first();

        $users = User::where('tenant_id', $tenant->id)->get();

        $roles = Role::all();

        return view('users.index')
            ->withUsers($users)
            ->withRoles($roles);
    }

    public function usersByRole($subdomain, $role_id)
    {
        $tenant = Tenant::where('subdomain', $subdomain)->first();

        $users = User::where([
            ['role_id', '=', $role_id],
            ['tenant_id', '=', $tenant->id]
        ])->get();

        $roles = Role::all();

        return view('users.index')
            ->withUsers($users)
            ->withRoles($roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($subdomain, Request $request)
    {
        $this->validate($request, [
            'fullname' => 'required|max:255',
            'ktp_id' => 'required|unique:users',
            'username' => 'required|unique:users',
            'password' => 'required',
            'email' => 'required|unique:users',
            'phone' => 'required|unique:users',
        ]);

        $tenant = Tenant::where('subdomain', $subdomain)->first();

        $user = new User;

        $user->fullname = $request->fullname;
        $user->ktp_id = $request->ktp_id;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->birth_date = $request->birth_date;
        $user->phone = $request->phone;
        $user->mobile = $request->mobile;
        $user->mobile2 = $request->mobile2;
        $user->pin_bb = $request->pin_bb;
        $user->notes = $request->notes;

        $user->photo = $request->photo;

        $user->address = $request->address;
        $user->zipcode = $request->zipcode;
        $user->fb = $request->fb;
        $user->tw = $request->tw;
        $user->website = $request->website;
        $user->status = $request->status;
        $user->hobby = $request->hobby;
        $user->reason = $request->reason;

//        Default User Role (Student)
        $user->role_id = 5;

//        Default Branch
        $user->branch_id = 0;

        Log::debug('Tenant ID = ' . $tenant->id);
        $user->tenant_id = $tenant->id;

        $user->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
