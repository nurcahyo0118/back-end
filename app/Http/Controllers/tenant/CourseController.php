<?php

namespace App\Http\Controllers\tenant;

use App\Category;
use App\Course;
use App\Section;
use App\SubCategory;
use App\Tenant;
use App\Material;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Image;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($subdomain)
    {
        $categories = Category::where(['tenant_id' => Auth::user()->tenant['id']])->get();
        $courses = Course::where(['tenant_id' => Auth::user()->tenant['id']])->get();

        return view('courses.index')
            ->withCategories($categories)
            ->withCourses($courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($subdomain)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        $categories = Category::where(['tenant_id' => $tenant->id])->get();

        $sub_categories = SubCategory::all();

        return view('courses.create')
            ->withCategories($categories)
            ->withSubCategories($sub_categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($subdomain, Request $request)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        $http = new Client;

        $response = $http->request('POST', 'https://api.wistia.com/v1/projects.json', [
            'form_params' => [
                'name' => $request->title,
                'adminEmail' => Auth::user()->email,
                'api_password' => env('WISTIA_TOKEN')
            ]
        ]);

        Log::debug($response->getBody());

        $wistia_project = json_decode($response->getBody(), true);

        Log::debug('Hashed ID = ' . $wistia_project['hashedId']);

        $course = new Course;

        /* General */
        $course->title = $request->title;
        $course->description = $request->description;
        $course->category_id = $request->category_id;
        $course->price = $request->price;

        /* Detail */
        $course->suitable = $request->suitable;
        $course->requirement = $request->requirement;
        $course->can_be = $request->can_be;

        /* Image */
        if ($request->hasFile('image')) {
            if ($course->image != null) {
                if (file_exists('xxcourses/images/' . $course->image)) {
                    unlink(public_path('xxcourses/images/' . $course->image));
                }
            }
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('/xxcourses/images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $course->image = $filename;
        }

        /* Video */
        $course->video = $request->video;

        $course->wistia_hashed_id = $wistia_project['hashedId'];

        $course->duration = 0;
        $course->status = 'POSTED';
        $course->tenant_id = $tenant->id;
        $course->author_id = Auth::id();

        $course->save();

        return redirect()->route('courses.edit', [$_SERVER['HTTP_HOST'], $course->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($subdomain, $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($subdomain, $id)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        $course = Course::find($id);

        $categories = Category::where(['tenant_id' => $tenant->id])->get();

        $sub_categories = SubCategory::all();

        return view('courses.edit')
            ->withCourse($course)
            ->withCategories($categories)
            ->withSubCategories($sub_categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($subdomain, Request $request, $id)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        $course = Course::find($id);

        /* General */
        $course->title = $request->title;
        $course->description = $request->description;
        $course->category_id = $request->category_id;
        $course->price = $request->price;

        /* Detail */
        $course->suitable = $request->suitable;
        $course->requirement = $request->requirement;
        $course->can_be = $request->can_be;

//        dd($request);
        /* Image */
        if ($request->hasFile('image')) {
            if ($course->image != null) {
                if (file_exists('xxcourses/images/' . $course->image)) {
                    unlink(public_path('xxcourses/images/' . $course->image));
                }
            }
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('/xxcourses/images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $course->image = $filename;
        }

        /* Video */
        $course->video = $request->video;

        $course->duration = 0;
        $course->status = 'POSTED';
        $course->tenant_id = $tenant->id;
        $course->author_id = Auth::id();

        $course->save();

        return redirect()->route('courses.edit', [$_SERVER['HTTP_HOST'], $course->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($subdomain, $id)
    {
        $http = new Client;

        $course = Course::find($id);

        $response = $http->request('DELETE', 'https://api.wistia.com/v1/projects/' . $course->wistia_hashed_id . '.json', [
            'form_params' => [
                'api_password' => env('WISTIA_TOKEN')
            ]
        ]);

        if ($response->getStatusCode() === 200) {

          $sections = Section::where(['course_id' => $course->id])->get();

          foreach ($sections as $section) {
            $materials = Material::where(['section_id' => $section->id])->get();

            foreach ($materials as $material) {
              $material->delete();
              Quiz::where(['material_id' => $material->id])->delete();
            }

            $section->delete();
          }

          $course->delete();
        }
        else
        {
          return response()->json(['message' => 'Failed, cannot delete wistia project'], 500);
        }


        return redirect()->back();
    }

}
