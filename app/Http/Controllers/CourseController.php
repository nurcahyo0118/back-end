<?php

namespace App\Http\Controllers;

use App\Category;
use App\Course;
use App\SubCategory;
use App\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($domain)
    {
        $categories = Category::all();
        $courses = Course::all();

        return view('courses.index')
            ->withCategories($categories)
            ->withCourses($courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($domain)
    {
        $tenant = Tenant::where(['domain' => $domain])->first();

        $categories = Category::where(['tenant_id' => $tenant->id])->get();

        $sub_categories = SubCategory::all();

        return view('courses.create')
            ->withCategories($categories)
            ->withSubCategories($sub_categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
        $course = new Course;
        $course->title = $request->title;
        $course->description = $request->description;
        $course->duration = 0;
        $course->status = 'POSTED';
        $course->price = $request->price;
        $course->category_id = $request->category_id;
        $course->tenant_id = 0;
        $course->author_id = Auth::id();
        $course->save();

        return redirect()->route('courses.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $course = Course::find($id);

        $course->delete();

        return redirect()->back();
    }
}
