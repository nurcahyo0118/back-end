<?php

namespace App\Http\Controllers;

use App\Section;
use App\Material;
use App\Quiz;
use Illuminate\Http\Request;
use App\Http\Controllers\CourseController;

use GuzzleHttp\Client;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($domain)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($domain)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
        $sections = Section::where(['course_id' => $request->course_id])->get();

        $section = new Section;
        $section->title = $request->title;
        $section->description = $request->description;
        $section->line = $sections->count() + 1;
        $section->course_id = (int) $request->course_id;
        $section->wistia_project_id = $request->wistia_hashed_id;

        $section->save();

//        return response()->json($section, 200);

        return view('courses.childsection')->withSection($section);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {

      return $request->all();
      $section = Section::find($id);
      $section->title = $request->title;
      $section->description = $request->description;

      $section->save();

      return response()->json([
       'section_id' => $section->id,
       'view' => (string)view('courses.childsection')->withSection($section)
      ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $http = new Client;

        $section = Section::find($id);

        $materials = $section->materials;

        foreach ($materials as $material) {

          $response = $http->request('DELETE', 'https://api.wistia.com/v1/medias/' . $material->wistia_media_url . '.json', [
            'form_params' => [
              'api_password' => env('WISTIA_TOKEN')
            ]
          ]);

          if ($response->getStatusCode() === 200) {
            $material->delete();
            Quiz::where(['material_id' => $material->id])->delete();
            $section->delete();
          }
          else
          {
            return response()->json(['message' => 'Failed delete media from wistia'], 404);
          }

        }

    }

    public function deleteAll($domain, $id)
    {
        $sections = Section::where(['course_id' => $id])->get();

        foreach ($sections as $section) {

          $http = new Client;

          $materials = $section->materials;

          foreach ($materials as $material) {

            $response = $http->request('DELETE', 'https://api.wistia.com/v1/medias/' . $material->wistia_media_url . '.json', [
              'form_params' => [
                'api_password' => env('WISTIA_TOKEN')
              ]
            ]);

            if ($response->getStatusCode() === 200) {
              $material->delete();
              Quiz::where(['material_id' => $material->id])->delete();
              $section->delete();
            }
            else
            {
              return response()->json(['message' => 'Failed delete media from wistia'], 404);
            }

          }
        }
    }
}
