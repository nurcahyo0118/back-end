<?php

namespace App\Http\Controllers;

use Log;
use Illuminate\Http\Request;
use App\Material;
use App\Course;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MaterialController extends Controller
{
    public function postMaterial($domain, Request $request)
    {

      // return $request->file('file')->getClientOriginalName();
        $course = Course::find($request->course_id);

        $http = new Client;

        if ($request->hasFile('file')) {
          $response = $http->request('POST', 'https://upload.wistia.com?api_password=' . env('WISTIA_TOKEN'), [
              'multipart' => [
                [
                  'name' => $request->file('file')->getClientOriginalName(),
                  'contents' => fopen($request->file('file'), 'r')
                ],
                [
                  'name' => 'project_id',
                  'contents' => $course->wistia_hashed_id
                ],
                [
                  'name' => 'name',
                  'contents' => $request->title
                ],
                [
                  'name' => 'description',
                  'contents' => ''
                ]

              ]
          ]);

          Log::debug('Response Wistia = ' . $response->getBody());

          $material = new Material;

          if ($response->getStatusCode() === 200) {

            $wistia_media = json_decode($response->getBody(), true);

            $materials = Material::where(['section_id' => $request->section_id])->get();

            $material->title = $request->title;
            $material->line = $materials->count() + 1;
            $material->type = $request->type;
            $material->wistia_media_hashed_id = $wistia_media['hashed_id'];
            $material->wistia_project_id = $course->wistia_hashed_id;
            $material->section_id = $request->section_id;

            $material->save();

            return response()->json([
              'section_id' => $request->section_id,
              'view' => (string)view('courses.childmaterial')->withMaterial($material)
            ]);
          }
          else
          {
            return response()->toJson(['message' => 'Failed upload media to wistia'], 404);
          }

        }
        else
        {
          return response()->toJson(['message' => 'Cannot find file'], 404);
        }

    }

    public function deleteMaterial($domain, $id)
    {
        $http = new Client;

        $material = Material::find($id);

        if ($material != null) {
          $response = $http->request('DELETE', 'https://api.wistia.com/v1/medias/' . $material->wistia_media_url . '.json', [
            'form_params' => [
              'api_password' => env('WISTIA_TOKEN')
            ]
          ]);

          if ($response->getStatusCode() === 200) {
            // Jika Request berhasil
            $material->delete();
          }
          else {
            // Jika gagal
            return response()->json(['message' => 'Failed delete media from wistia'], 404);
          }

        }
        else
        {
          return response()->json(['message' => 'Cannot find material'], 404);
        }
    }

    public function funny()
    {
      return 'nose';
    }

}
