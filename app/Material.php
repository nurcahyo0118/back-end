<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    public function section()
    {
        return $this->hasOne('App\Section', 'id', 'section_id');
    }

    public function quizzes()
    {
        return $this->hasMany('App\Quiz', 'material_id');
    }
}
