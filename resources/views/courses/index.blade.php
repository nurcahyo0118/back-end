@extends('layouts.main')

@section('content')

@php
    $tenant = Auth::user()->tenant;
    $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
@endphp

    <div class="row">

        <div class="modal fade" id="modalAdd">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Tambah Bab</h4>
                    </div>

                    <form action="{{ route('courses.store', $_SERVER['HTTP_HOST']) }}" method="post"
                          onsubmit="onSubmitCourse()"
                          data-parsley-validate>

                        <div class="modal-body">
                            <fieldset>
                                {{ csrf_field() }}
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Judul : </label>
                                        <input class="form-control" type="text" name="title">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Deskripsi : </label>
                                        <textarea class="form-control" name="description" rows="3"></textarea>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label> Kategori : </label>
                                        <select class="form-control" name="category_id">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"> {{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label> Harga (Rp) : </label>
                                        <input class="form-control" type="number" name="price">
                                    </div>
                                </div>

                            </fieldset>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <i class="fa fa-close"></i>
                                Tutup
                            </button>
                            <button type="submit" class="btn btn-primary" onclick="addSection()">
                                <i class="fa fa-save"></i>
                                Simpan
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div class="col-xs-12">

            @include('layouts.partials.message')

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Daftar Kursus</h3>
                    <br>

                    <a class="btn btn-primary pull-left col-md-1" href="#!" data-toggle="modal" data-target="#modalAdd">
                        <span class="fa fa-plus"></span>
                    </a>
                    {{--<a class="btn btn-info pull-left col-md-1" href="{{ route('courses.create', $_SERVER['HTTP_HOST']) }}">--}}
                    {{--<span class="fa fa-plus"></span>--}}
                    {{--</a>--}}

                </div>
                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Judul</th>
                            <th>Durasi</th>
                            <th>Harga (Rp)</th>
                            <th>Instruktur</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($courses as $course)
                            <tr>
                                <td><b>{{ substr($course->title, 0, 20) }}</b></td>
                                <td class="text-green">{{ $course->duration }} Menit</td>
                                <td>Rp. {{ number_format($course->price, 0, ',', '.') }}</td>
                                <td>{{ \App\User::find($course->author_id)->fullname }}</td>
                                <td class="text-green">{{ $course->status }}</td>
                                <td class="text-right">

                                    {{--<a href="#!" class="btn btn-info btn-xs">--}}
                                        {{--<i class="fa fa-users"></i>--}}
                                        {{--Murid--}}
                                    {{--</a>--}}

                                    @if($permission->update_course)
                                        <a href="{{ route('courses.edit', [$_SERVER['HTTP_HOST'], $course->id]) }}"
                                           class="btn btn-default btn-xs">
                                            <i class="fa fa-gear"></i>
                                            Kelola
                                        </a>
                                    @endif

                                    @if($permission->delete_course)
                                        <a href="#!" data-toggle="modal" data-target="#modalDelete{{ $course->id }}"
                                           class="btn btn-danger btn-xs">
                                            <i class="fa fa-trash"></i>
                                            Hapus
                                        </a>
                                    @endif
                                </td>
                            </tr>

                            <div class="modal fade" id="modalDelete{{ $course->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="defaultModalLabel">Hapus {{ $course->name }}
                                                                                           ?</h4>
                                        </div>
                                        <div class="modal-body">
                                            Anda yakin ingin menghapus data ini?
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::open(['route' => ['courses.destroy', $_SERVER['HTTP_HOST'], $course->id], 'method' => 'DELETE']) !!}
                                            <button type="button" class="btn btn-default" data-dismiss="modal"> Batal
                                            </button>
                                            <button type="submit" class="btn btn-danger"> Hapus</button>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        function onSubmitCourse() {
            $.ajax({
                type: "POST",
                url: 'https://upload.wistia.com',
                data: {project_id:},
                success: function (response) {
                    console.log(response);

                    swal("Berhasil !", "Modul berhasil ditambahkan", "success");
                },
                error: function (error) {
                    swal("Gagal !", "Section gagal ditambahkan", "error");
                }
            });
        }
    </script>
@endsection
