@extends('layouts.main')

@section('content')

    <div class="row">

        <div class="modal fade" id="modalAddSection">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Tambah Bab</h4>
                    </div>

                    <form class="" action="{{ route('courses.store', $_SERVER['HTTP_HOST']) }}" method="post"
                          data-parsley-validate>

                        <div class="modal-body">
                            <fieldset>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Judul : </label>
                                        <input type="text" name="title" class="form-control" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Deskripsi : </label>
                                        <textarea type="text" name="description" class="form-control"
                                                  rows="4"></textarea>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <i class="fa fa-close"></i>
                                Tutup
                            </button>
                            <button type="submit" class="btn btn-primary" onclick="addSection()">
                                <i class="fa fa-save"></i>
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="col-xs-12">

            @include('layouts.partials.message')

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Buat Kursus</h3>
                    <br>
                </div>
                <div class="box-body">

                    <div class="board">

                        <div class="board-inner">
                            <ul class="nav nav-tabs">
                                <div class="liner"></div>
                                <li class="steps active">
                                    <a href="#general" data-toggle="tab" title="Umum">
                                        <span class="round-tabs one">
                                            <i class="fa fa-home"></i>
                                        </span>
                                    </a>
                                </li>
                                <li class="steps">
                                    <a href="#course" data-toggle="tab" title="Kursus">
                                        <span class="round-tabs two">
                                            <i class="fa fa-list-ol"></i>
                                        </span>
                                    </a>
                                </li>

                                <li class="steps">
                                    <a href="#quiz" data-toggle="tab" title="Kuis">
                                        <span class="round-tabs three">
                                            <i class="fa fa-question"></i>
                                        </span>
                                    </a>
                                </li>

                                <li class="steps">
                                    <a href="#exercise" data-toggle="tab" title="Latihan">
                                        <span class="round-tabs four">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                    </a>
                                </li>
                                <li class="steps">
                                    <a href="#reference" data-toggle="tab" title="Referensi">
                                        <span class="round-tabs five">
                                            <i class="fa fa-link"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="general">

                                <form id="course" action="{{ route('courses.store', $_SERVER['HTTP_HOST']) }}"
                                      method="post"
                                      enctype="multipart/form-data"
                                      data-parsley-validate>

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="row">
                                        <div class="col-md-3">
                                            <ul class="nav nav-pills nav-stacked admin-menu">
                                                <li class="active">
                                                    <a href="#" data-target-id="generals">
                                                        <i class="fa fa-file"></i>
                                                        Umum
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" data-target-id="details">
                                                        <i class="fa fa-archive"></i>
                                                        Detail
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" data-target-id="images">
                                                        <i class="fa fa-file-o fa-fw"></i>
                                                        Gambar
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" data-target-id="videos">
                                                        <i class="fa fa-bar-chart-o fa-fw"></i>
                                                        Video Promo
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>

                                        {{-- General --}}
                                        <div class="col-md-9 admin-content" id="generals">

                                            <fieldset>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Judul : </label>
                                                        <input class="form-control" type="text" name="title"
                                                               placeholder="Judul kursus" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Deskripsi : </label>
                                                        <textarea class="textarea" name="description"
                                                                  placeholder="Tuliskan deskripsi kursus anda disini"></textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label> Kategori : </label>
                                                        <select class="form-control" name="category_id" required>
                                                            @foreach($categories as $category)
                                                                <option value="{{ $category->id }}"> {{ $category->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label> Sub Kategori : </label>
                                                        <select class="form-control" name="sub_category_id">
                                                            @foreach($sub_categories as $sub_category)
                                                                <option value="{{ $sub_category->id }}"> {{ $sub_category->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label> Harga (Rp) : </label>
                                                        <input class="form-control" type="number" name="price">
                                                    </div>
                                                </div>

                                            </fieldset>

                                        </div>

                                        {{-- General --}}

                                        {{-- Detail --}}

                                        <div class="col-md-9 admin-content" id="details">

                                            <fieldset>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Siapa yang cocok : </label>
                                                        <textarea class="textarea" name="suitable"
                                                                  placeholder="Tuliskan siapa yang cocok dengan kursus anda disini"></textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Syarat : </label>
                                                        <textarea class="textarea" name="requirement"
                                                                  placeholder="Tuliskan syarat untuk mengikuti kursus anda disini"></textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Yang akan didapat murid : </label>
                                                        <textarea class="textarea" name="can_be"
                                                                  placeholder="Tuliskan apa yang akan murid dapat setelah mengikuti kursus anda disini"></textarea>
                                                    </div>
                                                </div>

                                            </fieldset>

                                        </div>

                                        {{-- Detail --}}

                                        {{-- Images --}}
                                        <div class="col-md-9 admin-content" id="images">

                                            <fieldset>
                                                <div class="col-md-12">
                                                    <label>Berkas ( Gambar ) : </label>
                                                    <div class="form-group">
                                                        <div class="btn btn-default btn-file">
                                                            <i class="fa fa-paperclip"></i>
                                                            Upload Gambar
                                                            <input type="file" name="image" accept="image/*">
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>

                                        </div>

                                        {{-- Images --}}

                                        {{-- Video --}}
                                        <div class="col-md-9 admin-content" id="videos">

                                        </div>
                                        {{-- Video --}}

                                    </div>

                                    <hr>

                                    <div class="col-md-12 text-right">
                                        <button type="button" class="btn btn-default">
                                            <i class="fa fa-close"></i>
                                            Tutup
                                        </button>
                                        <button type="submit" class="btn btn-primary" onclick="submit()">
                                            <i class="fa fa-save"></i>
                                            Simpan
                                        </button>
                                    </div>

                                </form>

                            </div>

                            <div class="tab-pane fade" id="course">

                                <div class="col-md-12">
                                    <button class="btn btn-primary">
                                        <i class="fa fa-plus"></i>
                                        Tambah Modul
                                    </button>
                                </div>

                                <br>
                                <br>

                                <div class="col-md-12">
                                    <div class="box box-primary box-solid collapsed-box">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Expandable</h3>

                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                    <i
                                                            class="fa fa-plus"></i>
                                                </button>

                                                <button type="button" class="btn btn-box-tool">
                                                    <i
                                                            class="fa fa-trash"></i>
                                                </button>
                                            </div>

                                        </div>

                                        <div class="box-body">
                                            <div class="col-md-12">
                                                <button class="btn btn-info">
                                                    <i class="fa fa-plus"></i>
                                                    Tambah Materi
                                                </button>
                                            </div>

                                            <br>
                                            <br>

                                            <div class="col-md-12">
                                                <div class="box box-info collapsed-box">
                                                    <div class="box-header with-border">

                                                        <h3 class="box-title">Materi 1 : </h3>
                                                        <input type="text" class="form-control">

                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool"
                                                                    data-widget="collapse">
                                                                <i class="fa fa-plus"></i>
                                                            </button>

                                                            <button type="button" class="btn btn-box-tool">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </div>

                                                    </div>

                                                    <div class="box-body">

                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="quiz">

                            </div>
                            <div class="tab-pane fade" id="exercise">

                            </div>
                            <div class="tab-pane fade" id="reference">

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>

        function addSection() {
            $.ajax({
                type: "POST",
                url: "",
                data: {}
            });
            $('#modalAddSection').modal('hide');
        }

        function submit() {
            $("#course").submit();
        }
    </script>

    <script>
        $(function () {
            $('.textarea').wysihtml5();
        })
    </script>

    <script>
        $(document).ready(function () {
            var navItems = $('.admin-menu li > a');
            var navListItems = $('.admin-menu li');
            var allWells = $('.admin-content');
            var allWellsExceptFirst = $('.admin-content:not(:first)');

            allWellsExceptFirst.hide();
            navItems.click(function (e) {
                e.preventDefault();
                navListItems.removeClass('active');
                $(this).closest('li').addClass('active');


                allWells.fadeOut();
                var target = $(this).attr('data-target-id');
                $('#' + target).fadeIn();
            });
        });
    </script>
@endsection
