@extends('layouts.main')

@section('content')

    <div class="row">

        <div class="col-xs-12">

            @include('layouts.partials.message')

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Daftar Kursus</h3>
                    <br>
                    <a class="btn btn-info pull-left col-md-1" href="{{ route('courses.create') }}">
                        <span class="fa fa-plus"></span>
                    </a>

                </div>
                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Judul</th>
                            <th>Durasi</th>
                            <th>Harga (Rp)</th>
                            <th>Instruktur</th>
                            <th>Status</th>
                            <th>##</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($courses as $course)
                            <tr>
                                <td><b>{{ substr($course->title, 0, 20) }}</b></td>
                                <td class="text-green">{{ $course->duration }} Menit</td>
                                <td>Rp. {{ number_format($course->price, 0, ',', '.') }}</td>
                                <td>{{ \App\User::find($course->author_id)->fullname }}</td>
                                <td class="text-green">{{ $course->status }}</td>
                                <td class="text-right">

                                    <a href="#!" class="btn btn-info btn-xs">
                                        <i class="fa fa-users"></i> Murid</a>

                                    <a href="#!" class="btn btn-default btn-xs">
                                        <i class="fa fa-gear"></i> Kelola</a>

                                    <a href="#!" data-toggle="modal" data-target="#modalDelete{{ $course->id }}"
                                       class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Hapus</a>
                                </td>
                            </tr>

                            <div class="modal fade" id="modalDelete{{ $course->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="defaultModalLabel">Hapus {{ $course->name }}
                                                ?</h4>
                                        </div>
                                        <div class="modal-body">
                                            Anda yakin ingin menghapus data ini?
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::open(['route' => ['courses.destroy', $course->id], 'method' => 'DELETE']) !!}
                                            <button type="button" class="btn btn-default" data-dismiss="modal"> Batal
                                            </button>
                                            <button type="submit" class="btn btn-danger"> Hapus</button>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection
