<div id="quiz{{ $quiz->id }}" class="col-md-12">
    <div class="box box-info collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Kuis {{ $quiz->line }} : </b></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool"
                        data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>

            </div>

        </div>

        <div class="box-body">
          <label>Kuis : </label>
          <p>{{ $quiz->question }}</p>

          <hr>

          <label>Pilihan : </label>
          <p>A : {{ $quiz->option_a }}</p>
          <p>B : {{ $quiz->option_b }}</p>
          <p>C : {{ $quiz->option_c }}</p>
          <p>D : {{ $quiz->option_d }}</p>

          <hr>

          <label>Jawaban</label>
          <p>{{ $quiz->option_true }}</p>
        </div>

        <div class="box-footer text-right">
          <button data-toggle="modal" data-target="#modalEditQuiz{{ $quiz->id }}" class="btn btn-default">Edit</button>
          <button onclick="onDeleteQuiz({{ $quiz->id }})" type="button" class="btn btn-danger">Hapus</button>
        </div>

    </div>

</div>

@include('courses.modal.editquiz')
