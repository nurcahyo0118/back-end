<div class="modal fade" id="modalEditQuiz{{ $quiz->id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Tambah Kuis</h4>
            </div>

            <form id="editquiz" enctype="multipart/form-data" data-parsley-validate>
                <div class="modal-body">
                    <fieldset>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <input type="hidden" name="id" value="{{ $quiz->id }}">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Soal Kuis : </label>
                                <!-- <input type="text" name="question" class="form-control" required> -->
                                <textarea name="question" class="form-control" required>{{ $quiz->question }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> A : </label>
                                <input type="text" name="option_a" class="form-control" value="{{ $quiz->option_a }}" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> B : </label>
                                <input type="text" name="option_b" class="form-control" value="{{ $quiz->option_b }}" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> C : </label>
                                <input type="text" name="option_c" class="form-control" value="{{ $quiz->option_c }}" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> D : </label>
                                <input type="text" name="option_d" class="form-control" value="{{ $quiz->option_d }}" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Jawaban : </label>
                                <select class="form-control" name="option_true">
                                  <option value="">Pilih jawaban</option>
                                  <option value="A" {{ $quiz->option_true === 'A' ? 'selected' : '' }}>A</option>
                                  <option value="B" {{ $quiz->option_true === 'B' ? 'selected' : '' }}>B</option>
                                  <option value="C" {{ $quiz->option_true === 'C' ? 'selected' : '' }}>C</option>
                                  <option value="D" {{ $quiz->option_true === 'D' ? 'selected' : '' }}>D</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                              <label> Waktu kuis : </label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="a" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="a" class="form-control" required>
                            </div>
                        </div>

                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                        Tutup
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
