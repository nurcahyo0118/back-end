<div class="modal fade" id="modalAddMaterial">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Tambah Materi</h4>
            </div>

            <form id="addmaterial" enctype="multipart/form-data" data-parsley-validate>
                <div class="modal-body">
                    <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <input id="course_id" type="hidden" name="course_id" value="{{ $section->course['id'] }}">

                        <input id="section_id" type="hidden" name="section_id" value="{{ $section->id }}">

                        <input id="type" type="hidden" name="type" value="VIDEO">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Judul : </label>
                                <input id="materialTitle" type="text" name="title"
                                       class="form-control" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Berkas : </label>
                                <input id="file" type="file" name="file" required>
                            </div>
                        </div>

                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                        Tutup
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </form>


        </div>
    </div>
</div>
