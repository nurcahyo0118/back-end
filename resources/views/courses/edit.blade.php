@extends('layouts.main')

@section('content')

    <div class="row">

      @include('courses.modal.addquiz')

        <div class="col-xs-12">

            @include('layouts.partials.message')

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Kelola Kursus</h3>
                    <br>
                </div>
                <div class="box-body">

                    <div class="board">

                        <div class="board-inner">
                            <ul class="nav nav-tabs">
                                <div class="liner"></div>
                                <li class="steps active">
                                    <a href="#generals" data-toggle="tab" title="Umum">
                                        <span class="round-tabs one">
                                            <i class="fa fa-home"></i>
                                        </span>
                                    </a>
                                </li>
                                <li class="steps">
                                    <a href="#courses" data-toggle="tab" title="Kursus">
                                        <span class="round-tabs two">
                                            <i class="fa fa-list-ol"></i>
                                        </span>
                                    </a>
                                </li>

                                <li class="steps">
                                    <a id="tab_quizzes" href="#quizzes" data-toggle="tab" title="Kuis">
                                        <span class="round-tabs three">
                                            <i class="fa fa-question"></i>
                                        </span>
                                    </a>
                                </li>

                                <li class="steps">
                                    <a href="#exercises" data-toggle="tab" title="Latihan">
                                        <span class="round-tabs four">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                    </a>
                                </li>
                                <li class="steps">
                                    <a href="#references" data-toggle="tab" title="Referensi">
                                        <span class="round-tabs five">
                                            <i class="fa fa-link"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="generals">

                                <form id="course"
                                      action="{{ route('courses.update', [$_SERVER['HTTP_HOST'], $course->id]) }}"
                                      method="post"
                                      enctype="multipart/form-data"
                                      data-parsley-validate>

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <input type="hidden" name="_method" value="put">

                                    <div class="row">
                                        <div class="col-md-3">
                                            <ul class="nav nav-pills nav-stacked admin-menu">
                                                <li class="active">
                                                    <a href="#" data-target-id="general">
                                                        <i class="fa fa-file"></i>
                                                        Umum
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" data-target-id="detail">
                                                        <i class="fa fa-archive"></i>
                                                        Detail
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" data-target-id="image">
                                                        <i class="fa fa-file-o fa-fw"></i>
                                                        Gambar
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" data-target-id="video">
                                                        <i class="fa fa-bar-chart-o fa-fw"></i>
                                                        Video Promo
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>

                                        {{-- General --}}
                                        <div class="col-md-9 admin-content" id="general">
                                            <fieldset>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Judul : </label>
                                                        <input class="form-control" type="text" name="title"
                                                               value="{{ $course->title }}"
                                                               placeholder="Judul kursus" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Deskripsi : </label>
                                                        <textarea class="textarea" name="description"
                                                                  placeholder="Tuliskan deskripsi kursus anda disini">{{ $course->description }}</textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label> Kategori : </label>
                                                        <select class="form-control" name="category_id" required>
                                                            @foreach($categories as $category)
                                                                <option value="{{ $category->id }}" {{ $category->id === $course->category_id ? 'selected' : '' }}> {{ $category->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label> Sub Kategori : </label>
                                                        <select class="form-control" name="sub_category_id">
                                                            @foreach($sub_categories as $sub_category)
                                                                <option value="{{ $sub_category->id }}"> {{ $sub_category->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label> Harga (Rp) : </label>
                                                        <input class="form-control" type="number" name="price"
                                                               value="{{ $course->price }}">
                                                    </div>
                                                </div>

                                            </fieldset>

                                        </div>
                                        {{-- General --}}

                                        {{-- Detail --}}
                                        <div class="col-md-9 admin-content" id="detail">

                                            <fieldset>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Siapa yang cocok : </label>
                                                        <textarea class="textarea" name="suitable"
                                                                  placeholder="Tuliskan siapa yang cocok dengan kursus anda disini">
                                                            {{ $course->suitable }}
                                                        </textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Syarat : </label>
                                                        <textarea class="textarea" name="requirement"
                                                                  placeholder="Tuliskan syarat untuk mengikuti kursus anda disini">
                                                            {{ $course->requirement }}
                                                        </textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Yang akan didapat murid : </label>
                                                        <textarea class="textarea" name="can_be"
                                                                  placeholder="Tuliskan apa yang akan murid dapat setelah mengikuti kursus anda disini">
                                                            {{ $course->can_be }}
                                                        </textarea>
                                                    </div>
                                                </div>

                                            </fieldset>

                                        </div>
                                        {{-- Detail --}}

                                        {{-- Images --}}
                                        <div class="col-md-9 admin-content" id="image">

                                            <fieldset>
                                                <div class="col-md-12 text-center">
                                                    @if( $course->image === null)
                                                        <img src="/xxcourses/images/default.png" class="img-responsive"
                                                             alt="">
                                                    @else
                                                        <img src="/xxcourses/images/{{ $course->image }}"
                                                             class="img-responsive" width="400" alt="">
                                                    @endif
                                                </div>

                                                <div class="col-md-12">
                                                    <label>Berkas ( Gambar ) : </label>
                                                    <div class="form-group">
                                                        <div class="btn btn-default btn-file">
                                                            <i class="fa fa-paperclip"></i>
                                                            Upload Gambar
                                                            <input type="file" name="image" accept="image/*">
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>

                                        </div>
                                        {{-- Images --}}

                                        {{-- Video --}}
                                        <div class="col-md-9 admin-content" id="video">

                                        </div>
                                        {{-- Video --}}

                                    </div>

                                    <hr>

                                    <div class="col-md-12 text-right">
                                        <a href="{{ route('courses.index', $_SERVER['HTTP_HOST']) }}"
                                           class="btn btn-default">
                                            <i class="fa fa-close"></i>
                                            Tutup
                                        </a>
                                        <button type="submit" class="btn btn-primary" onclick="submit()">
                                            <i class="fa fa-save"></i>
                                            Simpan
                                        </button>
                                    </div>

                                </form>

                            </div>

                            <div class="tab-pane fade" id="courses">

                                <div class="col-md-12">
                                    <button data-toggle="modal" data-target="#modalAddSection" class="btn btn-primary">
                                        <i class="fa fa-plus"></i>
                                        Tambah Modul
                                    </button>

                                    @include('courses.modal.addsection')

                                    <button onclick="onDeleteAllSection({{ $course->id }})"
                                            class="btn btn-danger pull-right">
                                        <i class="fa fa-trash"></i>
                                        Hapus Semua Modul
                                    </button>
                                </div>

                                <br>
                                <br>

                                <div id="sectionsList" class="col-md-12">
                                    @foreach($course->sections as $index => $section)

                                        @include('courses.childsection')

                                    @endforeach
                                </div>

                            </div>

                            <div class="tab-pane fade" id="quizzes">

                              <div class="col-md-12">

                                  <button onclick="onDeleteAllSection({{ $course->id }})"
                                          class="btn btn-danger pull-right">
                                      <i class="fa fa-trash"></i>
                                      Hapus Semua Kuis
                                  </button>
                              </div>

                              <br>
                              <br>

                              <div class="text-center loader">
                                <i class="fa fa-spinner fa-spin fa-5x"></i>
                              </div>

                              <div id="quizzesContent">

                              </div>
{{--
                              <div id="sectionsList" class="col-md-12">
                                  @foreach($course->sections as $index => $section)

                                      @include('courses.quiz.childsection')

                                      @include('courses.modal.addquiz')

                                  @endforeach
                              </div>
--}}
                            </div>
                            <div class="tab-pane fade" id="exercises">

                            </div>
                            <div class="tab-pane fade" id="references">

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script>
      $(function () {

        $(document).on('click', '#tab_quizzes', function () {

          $('#quizzesContent').empty();
          $('.loader').show();

          $.ajax({
              type: "GET",
              url: '/quizzes',
              success: function (response) {
                  $('.loader').hide();
                  for (var i = 0; i < response.length; i++) {
                    console.log(response[i].question);
                    $('#quizzesContent').append(response[i].question);
                  }
              },
              error: function (error) {
              }
          });

        });

        $(document).on('click', 'button#modaleditsection', function () {
          $('#section_id').val($(this).data('id'));
        });

        $(document).on('click', 'button#modaladdmaterial', function () {
          $('#section_id').val($(this).data('id'));
        });

        $(document).on('click', 'button#modaladdquiz', function () {
          $('#material_id').val($(this).data('id'));
        });

        $("form#addsection").submit(function(e) {
          e.preventDefault();
          var formData = new FormData(this);

          $.ajax({
              type: "POST",
              url: '/sections',
              data: formData,
              success: function (response) {
                  $('form#addsection')[0].reset();
                  $("#modalAddSection").modal('toggle');

                  $("#sectionsList").append(response);

                  swal("Berhasil !", "Modul berhasil ditambahkan", "success");
              },
              error: function (error) {
                  swal("Gagal !", "Modul gagal ditambahkan", "error");
              },
              cache: false,
              contentType: false,
              processData: false
          });

        });

        $(document).on('submit', 'form#editsection', function (event) {
          event.preventDefault();
          var formData = new FormData(this);

          console.log(formData.get('title'));
          console.log(formData.get('description'));

          $.ajax({
            url: '/sections/' + formData.get('section_id'),
            type: 'PUT',
            data: formData,
            success: function (data) {
              $('form#editsection')[0].reset();

              $("#modalEditSection" + formData.get('section_id')).modal('toggle');

              $("#section" + data['section_id']).remove();
              $("#section" + data['section_id']).append(data['view']);

              swal("Berhasil !", "Modul berhasil diedit", "success");
            },
            cache: false,
            contentType: false,
            processData: false
          });

        });

        $(document).on('submit', 'form#addmaterial', function (event) {
          event.preventDefault();
          var formData = new FormData(this);

          // swal("Gagal !", "Materi gagal ditambahkan", "error");

          $.ajax({
            url: '/material/store',
            type: 'POST',
            data: formData,
            success: function (data) {
              $('form#addmaterial')[0].reset();

              $("#modalAddMaterial").modal('toggle');

              $("#materiloadQuizzesalsList" + data['section_id']).append(data['view']);

              swal("Berhasil !", "Materi berhasil ditambahkan", "success");
            },
            cache: false,
            contentType: false,
            processData: false
          });

        });

        $(document).on('submit', 'form#addquiz', function (event) {
          event.preventDefault();

          var question = $('#quizquestion');
          var formData = new FormData(this);
          formData.append('question', question.val());


          $.ajax({
              url: '/quizzes',
              type: 'POST',
              data: formData,
              success: function (data) {
                  $('form#addquiz')[0].reset();

                  console.log(data['material_id']);
                  console.log(data['view']);

                  $("#quizzesList" + data['material_id']).append(data['view']);
                  $("#modalAddQuiz").modal('toggle');

                  swal("Berhasil !", "Kuis berhasil ditambahkan", "success");
              },
              cache: false,
              contentType: false,
              processData: false
          });

        });

        $("form#editquiz").submit(function(e) {
          e.preventDefault();
          var formData = new FormData(this);
          formData.append('question', 'pertanyaan');

          $.ajax({
              url: '/quizzes/' + formData.get('id'),
              type: 'PUT',
              data: formData,
              success: function (data) {
                  $('form#editquiz')[0].reset();
                  swal("Berhasil !", "Kuis berhasil ditambahkan", "success");
              },
              cache: false,
              contentType: false,
              processData: false
          });

        });

      });
    </script>
    <script>


        function onSubmit(id) {
            var title = $('#title');
            var description = $('#description');

            var formData = new FormData($('#addmaterial'));

            $.ajax({
                type: "POST",
                url: '/sections',
                data: formData,
                success: function (response) {
                    console.log(response);
                    $("#modalAddSection").modal('toggle');

                    $("#sectionsList").append(response);

                    title.val('');
                    description.val('');

                    swal("Berhasil !", "Modul berhasil ditambahkan", "success");
                },
                error: function (error) {
                    swal("Gagal !", "Section gagal ditambahkan", "error");
                }
            });

        }

        function onSelectCategory(select) {
            alert(select.value);
        }

        function submit() {
            $("#course").submit();
        }

        function onDeleteSection(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    deleteSection(id)
                }
            });
        }

        function onDeleteMaterial(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    deleteMaterial(id);
                }
            });
        }

        function onDeleteQuiz(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    deleteQuiz(id);
                }
            });
        }

        function onDeleteAllSection(courseId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    deleteAllSection(courseId);
                }
            });
        }

        /* Modal */
        function modalAddMaterial() {

        }

        /* Service */
        function deleteSection(id) {
            $.ajax({
                type: "DELETE",
                url: '/sections/' + id,
                success: function (response) {
                    console.log(response);
                    $("#section" + id).remove();
                    swal("Berhasil !", "Modul berhasil dihapus", "success");
                }
            });
        }

        function deleteMaterial(id) {
            $.ajax({
                type: "DELETE",
                url: '/material/delete/' + id,
                success: function (response) {
                    console.log(response);
                    $("#material" + id).remove();
                    swal("Berhasil !", "Materi berhasil dihapus", "success");
                }
            });
        }

        function deleteAllSection(courseId) {
            $.ajax({
                type: "DELETE",
                url: '/sections/delete-all/' + courseId,
                success: function (response) {
                    $("#sectionsList").empty();
                    swal("Berhasil !", "Semua Modul berhasil dihapus", "success");
                },
                error: function (error) {
                    swal("Gagal !", "Gagal menghapus semua modul", "error");
                }
            });
        }

        function deleteQuiz(id) {
            $.ajax({
                type: "DELETE",
                url: '/quizzes/' + id,
                success: function (response) {
                    console.log(response);
                    $("#quiz" + id).remove();
                    swal("Berhasil !", "Modul berhasil dihapus", "success");
                }
            });
        }
    </script>

    <script>
        $(function () {
            $('.textarea').wysihtml5();
        })
    </script>

    <script>
        $(document).ready(function () {
            var navItems = $('.admin-menu li > a');
            var navListItems = $('.admin-menu li');
            var allWells = $('.admin-content');
            var allWellsExceptFirst = $('.admin-content:not(:first)');

            allWellsExceptFirst.hide();
            navItems.click(function (e) {
                e.preventDefault();
                navListItems.removeClass('active');
                $(this).closest('li').addClass('active');


                allWells.fadeOut();
                var target = $(this).attr('data-target-id');
                $('#' + target).fadeIn();
            });

        });
    </script>

@endsection
