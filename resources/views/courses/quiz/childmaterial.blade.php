<div id="material{{ $material->id }}" class="col-md-12">

  <div class="box box-info box-solid collapsed-box">
    <div class="box-header with-border">
      <h3 class="box-title"><b>{{ $material->title }}</b></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool"
        data-widget="collapse">
        <i class="fa fa-plus"></i>
      </button>
    </div>

  </div>

  <div class="box-body">
    <label>Judul : </label>
    <div class="form-inline">
      <input type="text" value="{{ $material->title }}"
      class="form-control">
      <button type="button" class="btn btn-info">Submit</button>
    </div>

    <br>
    <hr>
    <div class="text-center">
      <button type="button" class="btn btn-info">
        <i class="fa fa-refresh"></i>
        Muat Video
      </button>
    </div>

    <br>
    <hr>

    <label>Daftar Kuis</label>

    <br>

    <button id="modaladdquiz" data-toggle="modal" data-id="{{ $material->id }}" data-target="#modalAddQuiz"
            class="btn btn-info">
        <i class="fa fa-plus"></i>
        Tambah Kuis
    </button>

    <br>
    <br>

    <div class="quizzesList{{ $material->id }}">
      @foreach($material->quizzes as $quiz)
        <div id="quiz{{ $material->id }}" class="col-md-12">
          <div class="box box-info box-solid collapsed-box">
            <div class="box-header with-border">

              <h3 class="box-title"><b>Kuis</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool"
                data-widget="collapse">
                <i class="fa fa-plus"></i>
              </button>
            </div>

          </div>

            <div class="box-body">
              <label>pertanyaan</label>
              <p>{{ $quiz->question }}</p>

              <hr>

              <label>pilihan</label>
              <p>A : {{ $quiz->option_a }}</p>
              <p>B : {{ $quiz->option_b }}</p>
              <p>C : {{ $quiz->option_c }}</p>
              <p>D : {{ $quiz->option_d }}</p>

              <hr>

              <label>Jawaban</label>
              <p>{{ $quiz->option_true }}</p>

              <hr>

              <label>Waktu ditampilkan</label>
              <p>05:10</p>
            </div>

            <div class="box-footer">
              <div class="col-md-12 text-right">
                <button class="btn btn-danger" type="button"
                        onclick="onDeleteMaterial({{ $material->id }})">
                  <i class="fa fa-trash"></i>
                  Hapus
                </button>
              </div>
            </div>
          </div>
        </div>
      @endforeach

    </div>

  </div>

  <div class="box-footer">
      <div class="col-md-12 text-right">
          <button class="btn btn-danger" type="button"
                  onclick="onDeleteMaterial({{ $material->id }})">
              <i class="fa fa-trash"></i>
              Hapus
          </button>
      </div>
  </div>

</div>

</div>
