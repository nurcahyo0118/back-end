<div id="quizSectionsList" class="col-md-12">
    @foreach($course->sections as $index => $section)

        @include('courses.quiz.childsection')

        @include('courses.modal.addquiz')

    @endforeach
</div>
