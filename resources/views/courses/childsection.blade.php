<div id="section{{ $section->id }}">
    <div class="box box-primary box-solid collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><b>{{ $section->title }}</b></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool"
                        data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>

        </div>

        <div class="box-body">
            <div class="col-md-12">

                <div class="form-group">
                    <label>Deskripsi : </label>
                    <p></p>
                    {{ $section->description }}
                </div>
            </div>

            <div class="col-md-12">
                <button id="modaladdmaterial" data-toggle="modal" data-id="{{ $section->id }}" data-target="#modalAddMaterial"
                        class="btn btn-info">
                    <i class="fa fa-plus"></i>
                    Tambah Materi
                </button>
                <br>
                <br>
            </div>

            <div id="materialsList{{ $section->id }}">
              @foreach($section->materials as $index => $material)
                @include('courses.childmaterial')
              @endforeach

            </div>

        </div>

        <div class="box-footer">
            <div class="col-md-12 text-right">
{{--
              <button id="modaleditsection" data-toggle="modal" data-id="{{ $section->id }}" data-target="#modalEditSection{{ $section->id }}" class="btn btn-default">
                  <i class="fa fa-edit"></i>
                  Edit
              </button>

--}}

              <button class="btn btn-danger" type="button"
                      onclick="onDeleteSection({{ $section->id }})">
                  <i class="fa fa-trash"></i>
                  Hapus
              </button>
            </div>
        </div>

    </div>
</div>
@include('courses.modal.addmaterial')

@include('courses.modal.editsection')
