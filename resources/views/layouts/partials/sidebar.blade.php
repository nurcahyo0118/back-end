<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <div class="user-panel">
            <div class="pull-left image">
                @if( Auth::user()->photo === null)
                    <img src="/images/default.png" class="img-circle" alt="User Image">
                @else
                    <img src="/images/photo/{{ Auth::user()->photo }}" class="img-circle" alt="User Image">
                @endif
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->fullname }}</p>
                <a>
                    <i class="fa fa-circle text-success"></i>
                    Online
                </a>
            </div>
        </div>

        {{-- Multi Domain --}}
        @if($permission != null)
            <ul class="sidebar-menu">
                <li>
                    <a href="{{ route('dashboard', '') }}">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                @if($permission->read_user)
                    <li class="header"> Kelola User</li>

                    <li class="active treeview">
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <span>User</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('users.index', '') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Semua
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 1]) }}">
                                    <i class="fa fa-circle-o"></i>
                                    SuperAdmin
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 2]) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Management
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 3]) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Expert
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 4]) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Instructor
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 5]) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Student
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif

                @if($permission->read_role)
                    <li>
                        <a href="{{ route('roles.index', '') }}">
                            <i class="fa fa-key"></i>
                            <span>Roles</span>
                        </a>
                    </li>
                @endif

                @if($permission->read_course || $permission->read_category)
                    <li class="header"> Kursus</li>
                @endif

                @if($permission->read_course)
                    <li>
                        <a href="{{ route('courses.index', $_SERVER['HTTP_HOST']) }}">
                            <i class="fa fa-book"></i>
                            <span>Kursus</span>
                        </a>
                    </li>
                @endif

                @if($permission->read_category)
                    <li>
                        <a href="{{ route('categories.index', $_SERVER['HTTP_HOST']) }}">
                            <i class="fa fa-list-ol"></i>
                            <span>Kategori</span>
                        </a>
                    </li>
                @endif

                @if($permission->read_tenant_me)
                    @if(Auth::user()->role_id === 3)
                        <li class="header"> Kelola Tenant</li>
                        <li>
                            <a href="{{ route('tenants.me', $_SERVER['HTTP_HOST']) }}">
                                <i class="fa fa-building"></i>
                                <span> Kelola Tenant</span>
                            </a>
                        </li>
                    @endif
                @endif

                @if($permission->read_tenant)
                    @if($_SERVER['HTTP_HOST'] === env('HOST_NAME'))
                        <li class="header"> Tenant</li>
                        <li>
                            <a href="{{ route('tenants.index', $_SERVER['HTTP_HOST']) }}">
                                <i class="fa fa-building"></i>
                                <span> Daftar Tenant</span>
                            </a>
                        </li>
                    @endif
                @endif

                <li class="header"> Pengaturan</li>
                <li class="active treeview">
                    <a href="#">
                        <i class="fa fa-gear"></i>
                        <span> Pengaturan</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('me', $_SERVER['HTTP_HOST']) }}">
                                <i class="fa fa-circle-o"></i>
                                Profil
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-circle-o"></i>
                                Ubah Kata Sandi
                            </a>
                        </li>

                    </ul>
                </li>

                <form id="logout"
                      action="{{ Auth::user()->tenant->is_cname ? route('logout', $_SERVER['HTTP_HOST']) : route('tenant.logout', Auth::user()->tenant->subdomain) }}"
                      method="post">
                    {{ csrf_field() }}
                </form>

                <li>
                    <a href="#" onclick="event.preventDefault(); document.getElementById('logout').submit();">
                        <i class="fa fa-sign-out"></i>
                        <span> Keluar</span>
                    </a>
                </li>
            </ul>
        @endif
        {{-- Multi Domain --}}

        {{-- Multi SubDomain --}}
        @if(false)
            <ul class="sidebar-menu">
                <li>
                    <a href="{{ route('dashboard', '') }}">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="header"> Kelola User</li>
                <li>
                    <a href="{{ route('roles.index', '') }}">
                        <i class="fa fa-key"></i>
                        <span>Roles</span>
                    </a>
                </li>
                <li class="active treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>User</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('users.index', '') }}">
                                <i class="fa fa-circle-o"></i>
                                Semua
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 1]) }}">
                                <i class="fa fa-circle-o"></i>
                                SuperAdmin
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 2]) }}">
                                <i class="fa fa-circle-o"></i>
                                Management
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 3]) }}">
                                <i class="fa fa-circle-o"></i>
                                Expert
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 4]) }}">
                                <i class="fa fa-circle-o"></i>
                                Instructor
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 5]) }}">
                                <i class="fa fa-circle-o"></i>
                                Student
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="header"> Kursus</li>
                <li class="active treeview">
                    <a href="#">
                        <i class="fa fa-book"></i>
                        <span> Kursus</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('courses.index', $_SERVER['HTTP_HOST']) }}">
                                <i class="fa fa-circle-o"></i>
                                Kursus
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('categories.index', $_SERVER['HTTP_HOST']) }}">
                                <i class="fa fa-circle-o"></i>
                                Kategori
                            </a>
                        </li>
                    </ul>
                </li>

                @if(Auth::user()->role_id === 3)
                    <li class="header"> Kelola Tenant</li>
                    <li>
                        <a href="{{ route('tenants.me', $_SERVER['HTTP_HOST']) }}">
                            <i class="fa fa-building"></i>
                            <span> Kelola Tenant</span>
                        </a>
                    </li>
                @endif

                @if($_SERVER['HTTP_HOST'] === env('HOST_NAME'))
                    <li class="header"> Tenant</li>
                    <li>
                        <a href="{{ route('tenants.index', $_SERVER['HTTP_HOST']) }}">
                            <i class="fa fa-building"></i>
                            <span> Daftar Tenant</span>
                        </a>
                    </li>
                @endif

                <li class="header"> Pengaturan</li>
                <li class="active treeview">
                    <a href="#">
                        <i class="fa fa-gear"></i>
                        <span> Pengaturan</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('me', $_SERVER['HTTP_HOST']) }}">
                                <i class="fa fa-circle-o"></i>
                                Profil
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-circle-o"></i>
                                Ubah Kata Sandi
                            </a>
                        </li>

                    </ul>
                </li>

                <form id="logout"
                      action="{{ !Auth::user()->tenant->is_cname ? route('tenant.logout', Auth::user()->tenant->subdomain) : route('logout', $_SERVER['HTTP_HOST']) }}"
                      method="post">
                    {{ csrf_field() }}
                </form>

                <li>
                    <a href="#" onclick="event.preventDefault(); document.getElementById('logout').submit();">
                        <i class="fa fa-sign-out"></i>
                        <span> Keluar</span>
                    </a>
                </li>
            </ul>
    @endif
    {{-- Multi Domain --}}



    <!-- <li class="treeview">
        <a href="#">
          <i class="fa fa-pie-chart"></i>
          <span>Charts</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
          <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
          <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
          <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
        </ul>
      </li> -->


        <!-- <li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
    </section>
</aside>
