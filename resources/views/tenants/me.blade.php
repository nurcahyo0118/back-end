@extends('layouts.main')

@section('content')

    <section class="content-header">
        <h1>
            Kelola Tenant
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">
                <i class="fa fa-building"></i>
                Kelola Tenant
            </li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle"
                             src="../../dist/img/user4-128x128.jpg" alt="User profile picture">

                        <h3 class="profile-username text-center"> {{ $tenant->name }}</h3>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b> Kursus</b>
                                <a class="pull-right">{{ $tenant->courses->count() }}</a>
                            </li>
                            <li class="list-group-item">
                                <b> Pengguna</b>
                                <a class="pull-right">{{ $tenant->users->count() }}</a>
                            </li>
                            <li class="list-group-item">
                                <b> Expert</b>
                                <a class="pull-right"> Tenant Expert</a>
                            </li>
                        </ul>

                        <a href="{{ route('tenants.me.edit', $_SERVER['HTTP_HOST']) }}"
                           class="btn btn-default btn-block"><b>
                                Edit</b></a>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"> Tentang</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <strong>
                            <i class="fa fa-globe margin-r-5"></i>
                            Laman</strong>

                        <p class="text-muted">
                            <a target="_blank"
                               href="#">{{ $tenant->subdomain }}
                                .babastudio.test
                            </a>
                        </p>

                        <hr>

                        <strong>
                            <i class="fa fa-file-text-o margin-r-5"></i>
                            Deskripsi</strong>

                        <p>{{ $tenant->description }}</p>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->

            <div class="col-md-9">
                <div class="box box-primary">

                    <div class="box-header">
                        <h3 class="box-title">Daftar User</h3>
                    </div>

                    <div class="box-body">
                        <table class="table js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Nama</th>
                                <th>E-Mail</th>
                                <th>Peran</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($tenant->users as $user)
                                <tr>
                                    <td>{{ $user->fullname }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role->name }}</td>

                                    <td class="text-right">
                                        {{--<a href="#!" class="btn btn-default btn-xs">--}}
                                            {{--<i class="fa fa-ban"></i>--}}
                                            {{--Tangguhkan--}}
                                        {{--</a>--}}
                                        <a href="#!" class="btn btn-danger btn-xs">
                                            <i class="fa fa-trash"></i>
                                            Hapus
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>

                </div>

            </div>


        </div>
        <!-- /.row -->

    </section>
@endsection
