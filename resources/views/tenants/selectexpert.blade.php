@extends('layouts.main')

@section('content')

    <section class="content-header">
        <h1>
            Pilih Expert
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{ route('tenants.index', $_SERVER['HTTP_HOST']) }}">
                    <i class="fa fa-building"></i>
                    Tenants
                </a>
            </li>
            <li>
                <a href="{{ route('tenants.show', [$_SERVER['HTTP_HOST'], $tenant->id]) }}">
                    <i class="fa fa-building"></i>
                    {{ $tenant->name }}
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-hand-o-right"></i>
                    Pilih Expert
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        <div class="row">

            <div class="col-md-12">
                <div class="box box-primary">

                    <div class="box-body">
                        <table class="table js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Nama</th>
                                <th>E-Mail</th>
                                <th>Peran</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($experts as $user)
                                <tr>
                                    <td>{{ $user->fullname }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role->name }}</td>

                                    <td class="text-right">
                                        <form action="{{ route('tenants.unselectExpert', [$_SERVER['HTTP_HOST'], $user->id]) }}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <input type="hidden" name="user_id" value="{{ $user->id }}">

                                            <button type="submit" class="btn btn-danger btn-xs">
                                                <i class="fa fa-hand-o-right"></i>
                                                Batalkan Pilihan
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->fullname }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role->name }}</td>

                                    <td class="text-right">
                                        <form action="{{ route('tenants.selectExpert', [$_SERVER['HTTP_HOST'], $id]) }}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <input type="hidden" name="user_id" value="{{ $user->id }}">

                                            <button type="submit" class="btn btn-success btn-xs">
                                                <i class="fa fa-hand-o-right"></i>
                                                Pilih
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>

                </div>

            </div>


        </div>
        <!-- /.row -->

    </section>
@endsection
