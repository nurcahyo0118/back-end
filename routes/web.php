<?php

Route::get('noPermission', 'DashboardController@noPermission')->name('noPermission');

Route::post('register', 'Auth\RegisterController@register');

Route::get('register', 'HomeController@register')->name('register');

Route::get('frontend', 'HomeController@index');

/* Tenant Routes */
Route::group(array(
    'domain' => '{subdomain}.' . env('HOST_NAME'),
    'as' => 'tenant.',
    'middleware' => 'multisubdomain'
), (function () {

    /* Auth Routes*/
    Route::get('/', [
        'as' => 'landing',
        'uses' => 'LoginController@getTenantLogin'
    ]);

    Route::post('/login', [
        'as' => 'login',
        'uses' => 'LoginController@tenantLogin'
    ]);

    Route::group(['middleware' => ['authen.tenant']], function () {

        Route::post('/logout', [
            'as' => 'logout',
            'uses' => 'LoginController@tenantLogout'
        ]);

        Route::get('/dashboard', [
            'as' => 'dashboard',
            'uses' => 'DashboardController@dashboard'
        ]);

        Route::get('me', [
            'as' => 'me',
            'uses' => 'ProfileController@index'
        ]);

        Route::get('me/edit', [
            'as' => 'me.edit',
            'uses' => 'ProfileController@edit'
        ]);

        Route::put('me/update', [
            'as' => 'me.update',
            'uses' => 'ProfileController@update'
        ]);

        Route::put('me/update-image', [
            'as' => 'me.update.image',
            'uses' => 'ProfileController@changeImage'
        ]);

        Route::put('me/update-password', [
            'as' => 'me.update.password',
            'uses' => 'ProfileController@changePassword'
        ]);

        /* My Tenant */

        Route::get('tenants/me', [
            'as' => 'tenants.me',
            'uses' => 'TenantController@me'
        ]);

        Route::get('tenants/me/edit', [
            'as' => 'tenants.me.edit',
            'uses' => 'TenantController@meEdit'
        ]);

        Route::get('tenants/{id}/select-expert', [
            'as' => 'tenants.selectExpertPage',
            'uses' => 'TenantController@selectExpertPage'
        ]);

        Route::post('tenants/{id}/select-expert', [
            'as' => 'tenants.selectExpert',
            'uses' => 'TenantController@selectExpert'
        ]);

        Route::post('tenants/{id}/unselect-expert', [
            'as' => 'tenants.unselectExpert',
            'uses' => 'TenantController@unselectExpert'
        ]);

        /* End My Tenant */

        Route::resource('tenants', 'tenant\TenantController');

        Route::resource('categories', 'tenant\CategoryController');

        Route::resource('sub_categories', 'tenant\SubCategoryController');

        Route::resource('users', 'tenant\UserController');

        Route::resource('roles', 'tenant\RoleController');

        Route::resource('sections', 'tenant\SectionController');

        Route::post('courses/{id}/section', [
            'as' => 'courses.add.section',
            'uses' => 'SectionController@store'
        ]);

        Route::resource('courses', 'tenant\CourseController');

        Route::get('users/role/{id}', [
            'as' => 'users.role',
            'uses' => 'UserController@usersByRole'
        ]);

        Route::put('user/update-role/{id}', [
            'as' => 'changeRole',
            'uses' => 'UserController@changeRole'
        ]);

        Route::post('users/select-expert', 'UserController@selectExpert')->name('users.selectExpert');

        Route::delete('sections/delete-all/{id}', [
            'as' => 'sections.delete.all',
            'uses' => 'SectionController@deleteAll'
        ]);

        Route::post('material/store', 'MaterialController@postMaterial');

        Route::delete('material/delete/{id}', 'MaterialController@deleteMaterial');

        Route::resource('quizzes', 'QuizController');

    });
    /* End Auth Routes*/

}));
/* end Tenant Routes */

Route::group(array('domain' => '{domain}'), (function () {

    Route::get('/', [
        'as' => '/',
        'uses' => 'LoginController@getLogin',
        'middleware' => 'multidomain'
    ]);

    Route::post('login', [
        'as' => 'login',
        'uses' => 'LoginController@postLogin'
    ]);

    Route::group(['middleware' => ['authen']], function () {

        Route::post('/logout', [
            'as' => 'logout',
            'uses' => 'LoginController@getLogout'
        ]);

        Route::get('/dashboard', [
            'as' => 'dashboard',
            'uses' => 'DashboardController@dashboard'
        ]);

        Route::get('me', [
            'as' => 'me',
            'uses' => 'ProfileController@index'
        ]);

        Route::get('me/edit', [
            'as' => 'me.edit',
            'uses' => 'ProfileController@edit'
        ]);

        Route::put('me/update', [
            'as' => 'me.update',
            'uses' => 'ProfileController@update'
        ]);

        Route::put('me/update-image', [
            'as' => 'me.update.image',
            'uses' => 'ProfileController@changeImage'
        ]);

        Route::put('me/update-password', [
            'as' => 'me.update.password',
            'uses' => 'ProfileController@changePassword'
        ]);

        /* My Tenant */

        Route::get('tenants/me', [
            'as' => 'tenants.me',
            'uses' => 'TenantController@me'
        ]);

        Route::get('tenants/me/edit', [
            'as' => 'tenants.me.edit',
            'uses' => 'TenantController@meEdit'
        ]);

        Route::get('tenants/{id}/select-expert', [
            'as' => 'tenants.selectExpertPage',
            'uses' => 'TenantController@selectExpertPage'
        ]);

        Route::post('tenants/{id}/select-expert', [
            'as' => 'tenants.selectExpert',
            'uses' => 'TenantController@selectExpert'
        ]);

        Route::post('tenants/{id}/unselect-expert', [
            'as' => 'tenants.unselectExpert',
            'uses' => 'TenantController@unselectExpert'
        ]);

        /* End My Tenant */

        Route::resource('tenants', 'TenantController');

        Route::resource('categories', 'CategoryController');

        Route::resource('sub_categories', 'SubCategoryController');

        Route::resource('users', 'UserController');

        Route::resource('roles', 'RoleController');

        Route::resource('sections', 'SectionController');

        Route::post('courses/{id}/section', [
            'as' => 'courses.add.section',
            'uses' => 'SectionController@store'
        ]);

        Route::resource('courses', 'CourseController');

        Route::get('users/role/{id}', [
            'as' => 'users.role',
            'uses' => 'UserController@usersByRole'
        ]);

        Route::put('user/update-role/{id}', [
            'as' => 'changeRole',
            'uses' => 'UserController@changeRole'
        ]);

        Route::post('users/select-expert', 'UserController@selectExpert')->name('users.selectExpert');

        Route::delete('sections/delete-all/{id}', [
            'as' => 'sections.delete.all',
            'uses' => 'SectionController@deleteAll'
        ]);

        Route::post('material/store', 'MaterialController@postMaterial');

        Route::delete('material/delete/{id}', 'MaterialController@deleteMaterial');

        Route::resource('quizzes', 'QuizController');

        // Route::post('quiz/store', 'QuizController@store');


        /* Append File */
//        Route::get('bangke', 'CategoryController@index');
        /* End Append File */
    });

}));


Route::get('/home', 'HomeController@index')->name('home');

Route::post('/users/add-role', 'UserController@addRole');

Route::post('/users/delete-role', 'UserController@deleteRole');

Route::get('404', [
    'as' => 'notfound',
    'uses' => 'HomeController@notfound'
]);
